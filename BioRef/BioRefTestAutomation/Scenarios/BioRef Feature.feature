﻿Feature: Bio Ref Tests


  Scenario Outline: ITSPM - 566
  Given The User Executes "<TestName>" and "<Description>"
  When The User has Launches the SPM application "<Division>"
  And The User logs-in to SPM application
  And The User enters begin order:
      |SpecimenTrackingID|BioRequisition|NamesMatch|TwoIDsOnSpecimen|Account|OrderType   |SpecimenCount|TestCount|CheckDigit|AccountNumber|
      |NOT RECEIVED      |Yes           |Yes       |Yes             |SG001  |MANUAL/QUICK|1            |1        |2         |2            |
  And The User enters order info page:
      |Priority|CollectionDate|TimeOfCollection|
      |ROUTINE |FASTING       |U               |
  And The User enter specimen details "<SpecimenCode>"
  And The User enters Test page "<TestCode>" and "<TestCodeConfirm>"

 
  
  Examples:
   |TestName|Description|Division                  |SpecimenCode|TestCode|TestCodeConfirm|
   |AC-1    |           |NJ1 (BRLI) - Clinical - 10|UGP         |1005-8  |10058          |

  
  
