﻿/****************************************************/
//Function Name: readExcelFile
//Created By: Yogeswaran B
//Created on: 14th Auguts 2020
//Description: This function helps to read the data from the excel file and convert it as dictionary object (key value pairs)
//Parameters: Excel Sheet path, Sheet Name
/****************************************************/
function readExcelFile(excelFileName, excelSheetName){
  
    //Creating a dictionary object
    let testData = getActiveXObject("Scripting.Dictionary");

    try{
    Log.Message("Successfully exporting excel values as Dictonary object from file "+excelFileName+" and from the sheet name "+excelSheetName);
    
    //Passing the Excel path and sheet name to read
    var Driver = DDT.ExcelDriver(excelFileName,excelSheetName);
    //Verifying excel sheet is having FieldName and Fieldvalue    
    if(Driver.ColumnCount==2){
    
    //verifying the pointer location
    while (!Driver.EOF()){
    
    //Getting the first Column
    var fName = DDT.CurrentDriver.Value(0);
    //Getting the second Column
    var fValue = DDT.CurrentDriver.Value(1);
    
    //Adding the first column as Key and second column as the Value
    testData.Add(fName,fValue);
    
    //Moving to the next row      
    Driver.Next();
    }    
    }else{
      
    Log.Error("Excel sheet is having less or more colums than 2");
    }
    
    DDT.CloseDriver(Driver.Name);
  }
  catch(e){
    
    Log.Error("Unable to locate the specified excel file or excel sheet in the location");
  }
  
  return testData;
}

/****************************************************/
//Function Name: getValuefrmExcel
//Created By: Yogeswaran B
//Created on: 17th Auguts 2020
//Description: This function helps to get a specific data from the excel file
//Parameters: Excel Sheet path, Query Name
//Limitation: It returns only a sinle string value from any given Excel file
/****************************************************/
function getValuefrmExcel(excelFileName, excelQuery){
  
    var val;
    try{
    Log.Message("Fetching excel value from the file "+excelFileName+" and using query "+excelQuery);
    //Get the Result Set using the getRecordSet function
    var rsObj = getRecordSet(excelFileName, excelQuery);
    
    //Verifying the pointer and moving it to the first
    if(rsObj.bof == false){
        rsObj.MoveFirst();
    }
    
    if(rsObj.fields.Count==2){
        
    Log.Message(rsObj.fields.Item(0));
    Log.Message(rsObj.fields.Item(1));
    val = aqConvert.VarToStr(rsObj.fields.Item(1));
    Log.Message(val)
    }
    }
    catch(e){
      Log.Error("Unable to locate the specified excel file or excel sheet in the location");
    }    
    finally{
      rsObj.close();
    }
    return val;
}

/****************************************************/
//Function Name: getRecordSet
//Created By: Yogeswaran B
//Created on: 17th Auguts 2020
//Description: This function helps to creates a ADODB recordset and returns the same
//Parameters: ExcelFilePath, ExcelQuery
//Limitation: none
/****************************************************/
function getRecordSet(excelFileName,excelQuery){
    //Declaring the Excel Connection
    var excelConnection = getActiveXObject("ADODB.Connection");
    //Establishing the connection using the Excel file name
    var str_Connection = getConnectionString(excelFileName);
    //Opening the connection
    excelConnection.Open(str_Connection);
    //Declaring the Record Set
    var excel_recordSet = getActiveXObject("ADODB.Recordset");
    //retriving the excel data using the query and excel connection details as a record set and returning the same
    excel_recordSet.Open(excelQuery, excelConnection);
    return excel_recordSet;
}

/****************************************************/
//Function Name: getConnectionString
//Created By: Yogeswaran B
//Created on: 17th Auguts 2020
//Description: This function helps to creates ADODB connection string and returns the same
//Parameters: ExcelFilePath
//Limitation: none
/****************************************************/
function getConnectionString(excelFileName){
  
    //Declaring the ADODB connection String according to the TestComplete bitness
    var ctExcelProvider64 = "Microsoft.ACE.OLEDB.12.0",
        ctExcelProvider32 = "Microsoft.Jet.OLEDB.4.0";
       
       Log.Message("Provider=" + (isTestComplete64Bit() ? ctExcelProvider64 : ctExcelProvider32) + ";Data Source = " + excelFileName + ";Persist Security Info=False;Extended Properties=Excel 8.0;");
    
    return "Provider=" + (isTestComplete64Bit() ? ctExcelProvider64 : ctExcelProvider32) + ";Data Source = " + excelFileName + ";Persist Security Info=False;Extended Properties=Excel 8.0;";
}

/****************************************************/
//Function Name: isTestComplete64Bit
//Created By: Yogeswaran B
//Created on: 17th Auguts 2020
//Description: This function helps to find the TestComplete Bitness
//Parameters: none
//Limitation: none
/****************************************************/
function isTestComplete64Bit(){
    //Verify whether TestComplete is running 
    if(Sys.Process("Test*te").WaitProperty("Exists",true)){
      //Verifies the bitness of the TestComplete
        return aqString.Compare(Sys.Process("Test*te").ProcessType,"x64",false) == 0;
    }
}

function readTestCodes(excelFileName, excelSheetName)
{
  var Driver;
  var arrayData = [];
  Driver = DDT.ExcelDriver(excelFileName,excelSheetName);

  while (! Driver.EOF() )
  {
    for(i = 0; i < DDT.CurrentDriver.ColumnCount; i++){
    arrayData.push(aqConvert.VarToStr(DDT.CurrentDriver.Value(i)));
    
  }
    Driver.Next();
  }

  DDT.CloseDriver(Driver.Name);
  return arrayData;
}