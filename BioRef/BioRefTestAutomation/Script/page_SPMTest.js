﻿//USEUNIT _commonFunctions
//USEUNIT quit_Browser

var process_SPM, beginOrder, txtBox_SPM, txtBoxConfirm, popUpDeleteTest, popUpCommaSeperatedCode, popUpOKBtn;
var popupErrorDeletedProfiles, popUpClickOk, table1;

var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
var path_AlertMessage = "WinFormsObject(\"frmMsgBox\")";
var path_AlertBtnOk = "WinFormsObject(\"btnOK\")";
var path_txtBoxConfirm = "WinFormsObject(\"txtConfirm\")";
var path_DeleteComponentTextBox="WinFormsObject(\"txtPromptData\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";
var path_AlertPopup = "WinFormsObject(\"txtMsg\")";
var path_popUpDeleteTest = "WinFormsObject(\"Panel2\")";
var path_Table = "WinFormsObject(\"frmMsgBox\")";
var path_popUpCommaSeperatedCode = "WinFormsObject(\"txtPromptData\")";
var path_popUpOKBtn = "WinFormsObject(\"btnOK\")";
var path_popupErrorDeletedProfiles = "WinFormsObject(\"txtMsg\")";


function enterval_Test(testCode, confirmTestCode){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    
    assertExists(path_textboxLabel,"*Tests:");
    _commonFunctions.setText(txtBox_SPM,testCode,"Test Code");
    _commonFunctions.actionTab(txtBox_SPM,"Test Code");
    
    txtBoxConfirm = process_SPM.Find("Name",path_txtBoxConfirm,10);
    
    if(txtBoxConfirm.Exists == true){
      
      _commonFunctions.setText(txtBoxConfirm,confirmTestCode,"Confirm TestCode");
      _commonFunctions.actionEnter(txtBoxConfirm,"Confirm TestCode");
    }
  
}

function tabTestCode(){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    
    assertExists(path_textboxLabel,"*Tests:");
    _commonFunctions.actionTab(txtBox_SPM,"Test Code");
   
}

function verify_AlertPopUp(){

  var Message;
  
  var AlertMsgBox = process_SPM.Find("Name", path_AlertPopup, 20);
    
 if(AlertMsgBox.Exists){
    Log.Checkpoint("Alert Message is present");
    
    Message = _commonFunctions.getText(AlertMsgBox, "Alert Text Message")
  } else {
    Log.Error("Expected Alert Popup is not present");
  }
  
  Log.Message(Message);
  
  return aqString.Trim(Message);
}

function deleteTestComponent(componentCode){

  process_SPM = Sys.Process("SPM");
  txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
  _commonFunctions.actionShiftTab(txtBox_SPM,"");
  _commonFunctions.actionControlD(txtBox_SPM,"");
   DeleteComponentTextBox=process_SPM.Find("Name",path_DeleteComponentTextBox,10);
   _commonFunctions.setText(DeleteComponentTextBox,componentCode,"Component Code");
    _commonFunctions.click(process_SPM.Find("Name",path_AlertBtnOk,10),"Ok Button");
    Log.Checkpoint("Successfully deleted Component");
    //_commonFunctions.actionTab(txtBox_SPM,"Tests");
}

function click_btnOk(){
  
      if(process_SPM.Find("Name",path_AlertBtnOk,10).Exists){
      
        _commonFunctions.click(process_SPM.Find("Name",path_AlertBtnOk,10),"Ok Button");            
    }
}
function assertExists(path,element){
    
    process_SPM.WaitWinFormsObject(path_textboxGeneric,1000);
    var elementPath = process_SPM.Find(["Name","WndCaption","VisibleOnScreen"],[path,element,"true"],10);
    aqUtils.Delay(1000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Test Page\"")
    }else{
      Log.Error("Element does not exist in the \"Test Page\"")
    }
}

function selectTest(columnName, columnValue){
  
  var grid, path_div, row_grid;
  process_SPM = Sys.Process("SPM");

  var path_div =  "WinFormsObject(\"dgvTests\")";
  grid = process_SPM.Find("Name",path_div,10);
  row_grid = _commonFunctions.select_RowFromDataGrid(grid, columnName, columnValue);
  
}

function deleteTest(columnValue){
   process_SPM = Sys.Process("SPM");
  var txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
  _commonFunctions.actionControlD(txtBox_SPM,columnValue);
}

function popupDeleteTest_Components(values){
  process_SPM = Sys.Process("SPM");
  popUpDeleteTest = process_SPM.Find("Name",path_popUpDeleteTest,10);
  popUpCommaSeperatedCode = process_SPM.Find("Name",path_popUpCommaSeperatedCode,10);
  popUpOKBtn = process_SPM.Find("Name",path_popUpOKBtn,10);
 
  if(popUpDeleteTest.Exists){
    
  _commonFunctions.setText(popUpCommaSeperatedCode,values,"Delete");
  _commonFunctions.click(popUpOKBtn,"OK Button");
  
  }
  
}


function popupDeletedProfilesComponents(){
  process_SPM = Sys.Process("SPM");
  popupErrorDeletedProfiles = process_SPM.Find("Name",path_popupErrorDeletedProfiles,10);
  
  var txt = popupErrorDeletedProfiles.Text;
    Log.Message(txt);

  table1 = process_SPM.Find("Name",path_Table,10);

  popUpClickOk = table1.Find("Name",path_popUpOKBtn,10);
  
  _commonFunctions.click(popUpClickOk,"Error message OK Button");

   _commonFunctions.click(txtBox_SPM,"");
   Log.Message("Pop up closed")
   
   return txt;

}

function closeAlertMessagePopUpAndMoveForward(){
  try{
    process_SPM = Sys.Process("SPM");
    alertMessageWindow = process_SPM.Find("Name",path_AlertMessage,10);
    alertMessageWindow.Close();
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    _commonFunctions.actionTab(txtBox_SPM,"Tests");
    }
    catch(e){
      Log.Message("Failed to Close Alert Message POP UP")
      Log.Error(e)
    }
  
}

function singleTabTestCode(){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    
    assertExists(path_textboxLabel,"*Tests:");
    _commonFunctions.actionTab(txtBox_SPM,"Specimen Code");
}