﻿When("The User has Launches the SPM application {arg}", function (division){
     try{
         SPM_Actions.LaunchSPMApplication(division);
         Log.Message("Successfully Launched SPM appliucation");
     }catch(exception)
     {
          Log.Error("Could not launch the SPM application, - " + exception);
     }   
});


When("The User logs-in to SPM application", function (){
     try{
          SPM_Actions.LogInToSPMApplication(_config.SPM_Username,_config.SPM_Password);
          Log.Message("Successfully Loggedin to SPM application.");
     }
     catch(err)
     {
          Log.Error("Could not login to SPM application." + err);
     }
     
});


When("The User enters begin order:", function (param1){
     let table = transformToTable(param1);
     SPM_Actions.FillBeginOrder(table);
});

When("The User enters order info page:", function (param1){
     let table = transformToTable(param1);
     SPM_Actions.FillingOrderInfoPage(table);
});



When("The User enter specimen details {arg}", function (specimenCode){
     SPM_Actions.EnterSpecimenDetails(specimenCode)
});

When("The User enters Test page {arg} and {arg}", function (testCode,testCodeConfirm){
     SPM_Actions.EnterTestPage(testCode,testCodeConfirm);
});
