﻿//USEUNIT _commonFunctions
var browser;
var path_sltBoxUpdateReason = "Select(\"ddlUpdateReason\")";
var path_btnOk = "SubmitButton(\"btnOK\")";
var path_listTests = "Panel(\"lstTests\")";
var path_Cell = "Cell(0, 1)";
var path_panel = "Panel(0)";
var path_chkBox = "Checkbox(\"*AddDeleteTests*\")";
var path_btnDelete = "SubmitButton(\"btnDelete\")";
var path_txtAddTestCode = "Textbox(\"txtTestCode\")";
var path_btnAdd = "SubmitButton(\"btnAdd\")";
var path_alertMSG = "Panel(\"alert_msg_add_delete_tests\")";
var path_btnNext = "SubmitButton(\"btnNext\")";
var path_btnSubmit = "SubmitButton(\"btnNext\")";
var path_updateSummary = "Panel(\"pnlUpdateSummary\")";
var path_popupAlert = "Panel(\"modal_finish\")";
var path_btnOk = "SubmitButton(\"btnOK\")";
var path_btnCancel = "SubmitButton(\"btnCancel\")";
var path_panelAddDelete = "Panel(\"pnlAddDeleteTests\")";
var path_panel2 = "Panel(2)";//TextNode("lblMessage")
var path_selectSpecimens="Select(\"ddlSpecimens\")"
var path_ManageSpecimens="Link(2)"
var path_panelSpecimensAdd="Panel(\"CancelNextControl_To_Move_From\")"

function select_UpdateReason(value){
    aqUtils.Delay(10000);
    _commonFunctions.clickItem(getElement("Name",path_sltBoxUpdateReason),value);
}

function enterval_TestCode(value){
  
   _commonFunctions.setText(getElement("Name",path_txtAddTestCode),value,"TestCode");
}

function click_btnOk(){
  
    _commonFunctions.click(getElement("Name",path_btnOk),"Ok button");
}

function click_btnDelete(){
  
    _commonFunctions.click(getElement("Name",path_btnDelete),"Delete button");
}

function click_btnAdd(){
  
    _commonFunctions.click(getElement("Name",path_btnAdd),"Add button");
}

function click_btnNext(){
    
    var panelAddDelete = getElement("Name",path_panelAddDelete);
    var btnNext = panelAddDelete.Find(["Name"],[path_btnNext],5);
    _commonFunctions.click(btnNext,"Next button");
}

function click_btnSubmit(){
    
    aqUtils.Delay(10000);
    var panelUpdateSummary = getElement("Name",path_updateSummary);
    var submitBtn = panelUpdateSummary.Find(["Name"],[path_btnSubmit],5);
    _commonFunctions.click(submitBtn,"Submit button");
}

function click_btnAlertOk(){
  
    aqUtils.Delay(5000);
    var panelMessageBox = getElement("Name",path_popupAlert);
    var okBtn = panelMessageBox.Find(["Name"],[path_btnOk],5);
    _commonFunctions.click(okBtn,"Alert Ok button");
}

function click_btnAlertCancel(){
  
    aqUtils.Delay(10000);
    var panelMessageBox = getElement("Name",path_popupAlert);
    var panel = panelMessageBox.Find(["Name"],[path_panel2],5);
    var cancelBtn = panel.Find(["Name"],[path_btnCancel],5);
    _commonFunctions.click(cancelBtn,"Alert Cancel button");
}

function getConfirmMessage(){
    aqUtils.Delay(5000);
    return (getElement("Name",path_alertMSG).contentText);
}

function verifyConfirmMessage(){
  
    aqUtils.Delay(10000);
    var panelMessageBox = getElement("Name",path_popupAlert);
    var confirmMessage = panelMessageBox.Find(["Name"],[path_panel],5);
    return (confirmMessage.contentText);
}

function selectTest(value){

   var flag = false;
   aqUtils.Delay(10000);
   var panel = getElement("Name",path_listTests);
   for(var i=0;i<panel.ChildCount;i++){
     
      if(aqString.Trim(panel.Child(i).Find(["Name"],[path_panel],5).contentText) == value){
      
          _commonFunctions.click(panel.Child(i).Find(["Name"],[path_chkBox],3), "Clicked on checkbox with value: "+value);
          flag = true;
          break;
      }
      }
      if(flag == false)
      Log.Error("Test Code "+value+" not present in the Accession");
   
}

function verifyTestPresent(value){

    var flag = false;
    aqUtils.Delay(10000);
   var panel = getElement("Name",path_listTests);
   for(var i=0;i<panel.ChildCount;i++){
     
      if(panel.Child(i).Find(["Name"],[path_panel],5).contentText == value){
      
          Log.Message("Test Code "+value+" found in the Accession");
          flag = true;
          break;
      }
      }
      if(flag == false)
      Log.Error("Test Code "+value+" not added in the Accession");
}

function getElement(prop,value){
  
  if(_config.browser == btIExplorer){
      
      browser = Sys.Browser("iexplore");
  }

    var element = browser.Find([prop,"VisibleOnScreen"],[value,"true"],20);
    
    for(var i=0;i<10;i++){
    if(!(browser.Find([prop],[value],20)).VisibleOnScreen){
    aqUtils.Delay(1000);}
    else{
      break;
    }
    }
        
    if(element.Exists){
      Log.Message("Element located in the \"Landing Page\"")
    }else{
      Log.Error("Element does not exist in the \"Landing Page\"")
    }
    
    return element;
}
function select_Specimens(value){
    aqUtils.Delay(10000);
    _commonFunctions.clickItem(getElement("Name",path_selectSpecimens),value);
}
function click_ManageSpecimens(){
    aqUtils.Delay(3000);
    _commonFunctions.click(getElement("Name",path_ManageSpecimens),"Manage Specimens");
}
function click_btnNextOnSpecimensAdd(){
    
    var panelAddSpecimens = getElement("Name",path_panelSpecimensAdd);
    var btnNext = panelAddDelete.Find(["Name"],[path_btnNext],5);
    _commonFunctions.click(btnNext,"Next button");
}
