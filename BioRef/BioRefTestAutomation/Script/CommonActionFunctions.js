﻿function transformToTable(rawData){

  let table = []; // Declaration of the resulting object

  // Iterate through rows.
  // We skip the first row (with index 0) that contains the column names
  for (let i = 1; i < rawData.RowCount; i++) {
    let row = {};
    
    // Iterate through cells of a row
    for (let j = 0; j < rawData.ColumnCount; j++) {
      // Add a property with the column name to the row object,
      // and assign the cell value to it
      row[rawData.Value(0, j)] = rawData.Value(i, j);
    }
    table.push(row); // Save data to the resulting object
  }
  return table;
}