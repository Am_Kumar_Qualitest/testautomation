﻿var FirstName, LastName, DOB, Gender, patientID, comment, DoctorName, Gender, AccessionID;
DoctorName = _commonFunctions.getDoctorName();
patientID = _commonFunctions.get4DigitRandomInt();
FirstName = _commonFunctions.randString(5);
LastName = _commonFunctions.randString(4);
comment = _commonFunctions.randString(4);
DOB = _commonFunctions.getDateOfBirth();
Gender = _commonFunctions.getGender();
Fasting = _commonFunctions.getFasting();
Race = _commonFunctions.getRace();
Ethincity = _commonFunctions.getEthnicity();

//Description : Launch SPM Application
function LaunchSPMApplication(Division)
{
         launch_Application.launchBrowser(_config.browser,_config.intranetLink);
         launch_Application.launch_App(_config.stage_SPM);
         launch_Application.clickRun(_config.popup_SPM);
         page_SPMLogin.select_Division(Division);
         page_SPMLogin.click_OkButton();
    
}

//Description Log into the SPM application
function LogInToSPMApplication(userName,password, division)
{
         page_SPMLogin.enterval_UserName(_config.Username)
         page_SPMLogin.enterval_Password(_config.Password)
         page_SPMLogin.click_LoginButton();
         page_SPMLogin.select_Division(division);
         page_SPMLogin.click_OkButton();
}

function FillBeginOrder(data)
{
     for (let i = 0; i < data.length; i++) 
     {
          page_SPMLanding.click_CreateOrder();
          page_SPMBeginOrder.enterval_SpecimenTrackingID(data[i].SpecimenTrackingID);
          page_SPMBeginOrder.enterval_BioRequisition(data[i].BioRequisition);
          page_SPMBeginOrder.enterval_NamesMatch(data[i].NamesMatch);
          page_SPMBeginOrder.enterval_TwoIDsOnSpecimen(data[i].TwoIDsOnSpecimen);
          page_SPMBeginOrder.enterval_Account(data[i].Account);
          page_SPMBeginOrder.enterval_OrderType(data[i].OrderType);
          page_SPMBeginOrder.enterval_SpecimenCount(data[i].SpecimenCount);
          page_SPMBeginOrder.enterval_TestCount(data[i].TestCount);
          page_SPMBeginOrder.popup_accountConfirmation(data[i].CheckDigit,data[i].AccountNumber);
     }   
}

function FillingOrderInfoPage(data)
{
     for (let i = 0; i < data.length; i++)
     {
          page_SPMOrderInfo.enterval_OrderingPhysician(DoctorName);
          page_SPMOrderInfo.enterval_Priority(data[i].Priority);
          page_SPMOrderInfo.enterval_PatientID(patientID);
          page_SPMOrderInfo.enterval_Comment(comment);
          page_SPMOrderInfo.enterval_LastName(LastName);
          page_SPMOrderInfo.enterval_FirstName(FirstName);
          page_SPMOrderInfo.enterval_MiddleName();
          page_SPMOrderInfo.enterval_Gender(Gender);
          page_SPMOrderInfo.enterval_DateOfCollection(data[i].CollectionDate);
          page_SPMOrderInfo.enterval_TimeOfCollection(data[i].TimeOfCollection);
          page_SPMOrderInfo.enterval_DateOfBirth(DOB);
          page_SPMOrderInfo.enterval_IsFastingSelection(Fasting);
     }
}

function EnterSpecimenDetails(specimen)
{
     page_SPMSpecimens.enterval_SpecimenCode(specimen);
     page_SPMSpecimens.doubleTabSpecimenCode();
}

function EnterTestPage(testCode,testCodeConfirm)
{
     page_SPMTest.enterval_Test(testCode,testCodeConfirm);
     page_SPMTest.tabTestCode();
     var ActualMessage = page_SPMTest.verify_AlertPopUp();
}