﻿//USEUNIT _commonFunctions
//USEUNIT _config

var process_SPM, additionalDemoInfo, txtBox_SPM;
var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";


function enterval_ClinicalInformation(value){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    assertExists("*Clinical Information*");
    _commonFunctions.setText(txtBox_SPM,value,"Clinical Information");
    _commonFunctions.actionTab(txtBox_SPM,"Clinical Information");
}

function enterval_PatientAddress(value){
  
    assertExists("*Patient Address:");
    _commonFunctions.setText(txtBox_SPM,value,"Patient Address");
    _commonFunctions.actionTab(txtBox_SPM,"Patient Address");
}

function enterval_ZIP(value){
  
    assertExists("*ZIP:");
    _commonFunctions.setText(txtBox_SPM,value,"ZIP");
    _commonFunctions.actionTab(txtBox_SPM,"ZIP");
}

function enterval_City(value){
  
    assertExists("*City:");
    _commonFunctions.setText(txtBox_SPM,value,"City");
    _commonFunctions.actionTab(txtBox_SPM,"City");
}
function enterval_State(value){
  
    assertExists("*State:");
    _commonFunctions.setText(txtBox_SPM,value,"State");
    _commonFunctions.actionTab(txtBox_SPM,"State");
}
function enterval_PatientPhone(value){
  
    assertExists("*Patient Phone:");
    _commonFunctions.setText(txtBox_SPM,value,"Patient Phone:");
    _commonFunctions.actionTab(txtBox_SPM,"Patient Phone:");
}
function assertExists(element){
    process_SPM.WaitWinFormsObject(path_textboxGeneric,1000);
    var elementPath = process_SPM.Find(["Name","VisibleOnScreen"],[path_textboxLabel,"true"],20);
    elementPath.WaitProperty("WndCaption",element,2000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Additional Demographic Info\"")
    }else{
      Log.Error("Element does not exist in the \"Additional Demographic Info\"")
    }
}