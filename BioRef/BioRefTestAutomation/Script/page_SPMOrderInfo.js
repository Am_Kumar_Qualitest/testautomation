﻿//USEUNIT quit_Browser
//USEUNIT _commonFunctions

var process_SPM, orderInfo, txtBox_SPM;
var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";
var path_duplicateOrder = "WinFormsObject(\"frmMsgBox\")";

/****************************************************/
//Function Name: enterval_OrderingPhysician
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in OrderingPhysician
//Parameters: value
/****************************************************/
function enterval_OrderingPhysician(value){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    assertExists("*Ordering Physician:");
    _commonFunctions.setText(txtBox_SPM,value,"OrderingPhysician");
    _commonFunctions.actionTab(txtBox_SPM,"OrderingPhysician");
}
/****************************************************/
//Function Name: enterval_Priority
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in priority
//Parameters: value
/****************************************************/
function enterval_Priority(value){
  
    assertExists("*Priority:");
    _commonFunctions.setText(txtBox_SPM,value,"Priority");
    _commonFunctions.actionTab(txtBox_SPM,"Priority");
}
/****************************************************/
//Function Name: enterval_PatientID
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in PatientID
//Parameters: value
/****************************************************/
function enterval_PatientID(value){
    
    assertExists("*Patient ID:");
    _commonFunctions.setText(txtBox_SPM,value,"PatientID");
    _commonFunctions.actionTab(txtBox_SPM,"PatientID");
}
/****************************************************/
//Function Name: enterval_Comment
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in comment
//Parameters: value
/****************************************************/
function enterval_Comment(value){
  
    assertExists("*Comment:");
    _commonFunctions.setText(txtBox_SPM,value,"Comment");
    _commonFunctions.actionTab(txtBox_SPM,"Comment");
}
/****************************************************/
//Function Name: enterval_LastName      
//Created By: Raja singh 
//Created on: 
//Description: This function helps to entering value in LastName
//Parameters: value
/****************************************************/
function enterval_LastName(value){
  
    assertExists("*Last Name:");
    _commonFunctions.setText(txtBox_SPM,value,"LastName");
    _commonFunctions.actionTab(txtBox_SPM,"LastName");
}
/****************************************************/
//Function Name: enterval_FirstName
//Created By: Raja singh 
//Created on: 
//Description: This function helps to entering value in FirstName
//Parameters: value
/****************************************************/
function enterval_FirstName(value){
  
    assertExists("*First Name:");
    _commonFunctions.setText(txtBox_SPM,value,"FirstName");
    _commonFunctions.actionTab(txtBox_SPM,"FirstName");
}
/****************************************************/
//Function Name: enterval_MiddleName
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in MiddleName
//Parameters: value
/****************************************************/
function enterval_MiddleName(){
  
    assertExists("*Middle Name/Initial:");
    //_commonFunctions.setText(txtBox_SPM,value);
    _commonFunctions.actionTab(txtBox_SPM,"MiddleName");
}
/****************************************************/
//Function Name: enterval_Gender
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in Gender
//Parameters: value
/****************************************************/
function enterval_Gender(value){
  
    assertExists("*Gender:");
    _commonFunctions.setText(txtBox_SPM,value,"Gender");
    _commonFunctions.actionTab(txtBox_SPM,"Gender");
}
/****************************************************/
//Function Name: enterval_DateOfCollection
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in DateOfCollection
//Parameters: value
/****************************************************/
function enterval_DateOfCollection(value){
  
    assertExists("*Date of Collection:");
    _commonFunctions.setText(txtBox_SPM,value,"DateOfCollection");
    _commonFunctions.actionTab(txtBox_SPM,"DateOfCollection");
}
/****************************************************/
//Function Name: enterval_TimeOfCollection
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in TimeOfcollection
//Parameters: value
/****************************************************/
function enterval_TimeOfCollection(value){

    assertExists("*Time of Collection:"); 
    _commonFunctions.setText(txtBox_SPM,value);
    _commonFunctions.actionTab(txtBox_SPM,"TimeOfCollection");
}
/****************************************************/
//Function Name: enterval_DateOfBirth
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in Date of birth field
//Parameters: value
/****************************************************/
function enterval_DateOfBirth(value){

    assertExists("*DOB:"); 
    _commonFunctions.setText(txtBox_SPM,value,"DateOfBirth");
    _commonFunctions.actionTab(txtBox_SPM,"DateOfBirth");
}
/****************************************************/
//Function Name: enterval_IsFastingSelection
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in IsFastingSelection
//Parameters: value
/****************************************************/
function enterval_IsFastingSelection(value){

    assertExists("*Fasting:"); 
    _commonFunctions.setText(txtBox_SPM,value,"IsFastingSelection");
    _commonFunctions.actionTab(txtBox_SPM,"IsFastingSelection");
}
/****************************************************/
//Function Name: enterval_Age
//Created By: Raja singh 

//Created on: 
//Description: This function helps to entering value in Age
//Parameters: value
/****************************************************/
function enterval_Age(){

    if(assertExists("*Age*").Exists){   
    //_commonFunctions.setText(txtBox_SPM,value);
    _commonFunctions.actionTab(txtBox_SPM,"Age");
    }
}

function assertExists(element){
    
    process_SPM.WaitWinFormsObject(path_textboxGeneric,1000);
    var elementPath = process_SPM.Find(["Name","WndCaption","VisibleOnScreen"],[path_textboxLabel,element,"true"],10);
    elementPath.WaitProperty("WndCaption",element,5000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Order Information Page\"")
    }else{
      Log.Error("Element does not exist in the \"Order Information Page\"")
    }
}   

function clicking_OnOrderInfo(){
 
    var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);
    var tabControl = table.TabControl;
    tabControl.ClickTab(" Order Info");//  Review Order  
    Log.Checkpoint("Successfully clicked on Order Info")
}
function enterval_UpdateFirstAndSecondName(lastName,firstName)
{
  try{
   process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    _commonFunctions.actionTab(txtBox_SPM,"OrderingPhysician");
    _commonFunctions.actionTab(txtBox_SPM,"Priority");
    _commonFunctions.actionTab(txtBox_SPM,"PatientID");
     //_commonFunctions.actionTab(txtBox_SPM,"Comment");
     _commonFunctions.setText(txtBox_SPM,"","LastName");
     _commonFunctions.setText(txtBox_SPM,lastName,"LastName");
     _commonFunctions.actionTab(txtBox_SPM,"LastName");
      _commonFunctions.setText(txtBox_SPM,"","FirstName");
     _commonFunctions.setText(txtBox_SPM,firstName,"FirstName");
     _commonFunctions.actionTab(txtBox_SPM,"FirstName");
     Log.Message("Successfully update First Name and Last Name")
     }
     catch(e){
       Log.Message("Failed to update First Name and Last Name")
       Log.Error(e)
     }
  
}
function enterval_SurgicalNumber(value){

    assertExists("*Surgical Number:"); 
    _commonFunctions.setText(txtBox_SPM,value,"Surgical Number");
    _commonFunctions.actionTab(txtBox_SPM,"Surgical Number");
}
function enterval_ReferringPhysician(value){

    assertExists("*Referring Physician:");
    _commonFunctions.setText(txtBox_SPM,value,"ReferringPhysician");
    _commonFunctions.actionDoubleTab(txtBox_SPM,"ReferringPhysician");
}
function enterval_PathologyTimeOfCollection(value){

    assertExists("*Time of Collection:"); 
    _commonFunctions.setText(txtBox_SPM,value);
    _commonFunctions.actionTab(txtBox_SPM,"TimeOfCollection");
}
function verifyMergePopUpNotExist(){
  process_SPM = Sys.Process("SPM");
    mergeMessage = process_SPM.Find("Name",path_duplicateOrder,10);
    if(!mergeMessage.Exists){
      Log.Checkpoint("Successfully verified that the user is  NOT presented with the merge message")
    }
    else{
      Log.Error("User is presented with merge message")
    }
}
function verifyMergePopUpExist(){
  process_SPM = Sys.Process("SPM");
    mergeMessage = process_SPM.Find("Name",path_duplicateOrder,10);
    if(mergeMessage.Exists){
      Log.Checkpoint("Successfully verified that the user is presented with the merge message")
    }
    else{
      Log.Error("User is NOT presented with merge message")
    }
}

