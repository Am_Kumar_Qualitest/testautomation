﻿//USEUNIT quit_Browser
//USEUNIT _commonFunctions

var process_TMA;
var path_MenuStrip = "WinFormsObject(\"MenuStrip\")";
var path_popupLogin = "WinFormsObject(\"frmLogin\")";


/****************************************************/
//Function Name: click_CreateOrder
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to clicking on Create order
//Parameters: 
/****************************************************/
function click_Database(){
  
  process_TMA = Sys.Process("TestMaster");
  process_TMA.WaitWinFormsObject("MenuStrip","MenuStrip",5000);
  Parent = process_TMA.Find("Name","WinFormsObject(\"frmMDIParent\")",2);
  Menu = Parent.StripMainMenu;
  
  Menu.Click("Database");
  
  Log.Message("Successfully Clicked on the \"Database\" button in menu strip");
}

function click_TestMaster(){
  
  process_TMA = Sys.Process("TestMaster");
  Parent = process_TMA.Find("Name","WinFormsObject(\"frmMDIParent\")",2);
  Menu = Parent.StripMainMenu;
  
  Menu.Click("Database|Test Master");
  
  Log.Message("Successfully Clicked on the \"Test Master\" button in option");
}