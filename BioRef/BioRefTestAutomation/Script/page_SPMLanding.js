﻿//USEUNIT quit_Browser
//USEUNIT _commonFunctions

var process_SPM, parent_SPM;
var path_CreateOrder = "WinFormsObject(\"ToolStrip\")";
var path_popupLogin = "WinFormsObject(\"frmLogin\")";


/****************************************************/
//Function Name: click_CreateOrder
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to clicking on Create order
//Parameters: 
/****************************************************/
function click_CreateOrder(){
  
  process_SPM = Sys.Process("SPM");
  CreateOrder = process_SPM.Find("Name",path_CreateOrder,10);
  LoginPopup = process_SPM.Find("Name",path_popupLogin,10);

  do{
  aqUtils.Delay(1000);
  if(!path_popupLogin.Exists){
    break;
  }
  }while(path_popupLogin.Exists);
  
  CreateOrder.ClickItem("Create Order");
  
  Log.Message("Successfully Clicked on the Create Order button in tool strip");
}

/****************************************************/
//Function Name: click_CreateOrder
//Created By: Raja singh 
//Created on: 
//Description: This function helps to clicking on Search
//Parameters: 
/****************************************************/
function click_OnSearch(){
  
  process_SPM = Sys.Process("SPM");
  search = process_SPM.Find("Name",path_CreateOrder,10);
  LoginPopup = process_SPM.Find("Name",path_popupLogin,10);

  do{
  aqUtils.Delay(1000);
  if(!path_popupLogin.Exists){
    break;
  }
  }while(path_popupLogin.Exists);
  
  search.ClickItem("Search");
  
  Log.Message("Successfully clicked on the Search button in tool strip");

}

