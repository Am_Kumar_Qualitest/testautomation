﻿//USEUNIT _commonFunctions
//USEUNIT page_SPMTest
//USEUNIT quit_Browser

var process_SPM, beginOrder, txtBox_SPM;
var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";

function enterval_SpecimenCode(value){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    
    assertExists("*Specimens:");
    _commonFunctions.setText(txtBox_SPM,value,"Specimen Code");
    _commonFunctions.actionTab(txtBox_SPM,"Specimen Code");
}
function doubleTabSpecimenCode(){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    
    assertExists("*Specimens:");
    _commonFunctions.actionTab(txtBox_SPM,"Specimen Code");
    _commonFunctions.actionTab(txtBox_SPM,"Specimen Code");
}
function enterval_MultipleSpecimenCode(value){
  
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);   
    _commonFunctions.setText(txtBox_SPM,value,"Specimen Code");
    _commonFunctions.actionTab(txtBox_SPM,"Specimen Code");
    
}

function deleteSpecimen(columnValue){
   process_SPM = Sys.Process("SPM");
  var txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
  _commonFunctions.actionControlD(txtBox_SPM,columnValue);
}

function popupDeleteSpecimen(values){
  process_SPM = Sys.Process("SPM");
  popUpDeleteTest = process_SPM.Find("Name",path_popUpDeleteTest,10);
  popUpCommaSeperatedCode = process_SPM.Find("Name",path_popUpCommaSeperatedCode,10);
  popUpOKBtn = process_SPM.Find("Name",path_popUpOKBtn,10);
 
  if(popUpDeleteTest.Exists){
    
  _commonFunctions.setText(popUpCommaSeperatedCode,values,"Delete");
  _commonFunctions.click(popUpOKBtn,"OK Button");
  
  }
  
}


function assertExists(element){
    
    process_SPM.WaitWinFormsObject(path_textboxGeneric,1000);
    var elementPath = process_SPM.Find(["Name","WndCaption","VisibleOnScreen"],[path_textboxLabel,element,"true"],10);
    elementPath.WaitProperty("WndCaption",element,2000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Specimen Page\"")
    }else{
      Log.Error("Element does not exist in the \"Specimen Page\"")
    }
}
function clicking_OnSpecimens(){
   var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);
    var tabControl = table.TabControl;
    tabControl.ClickTab("Specimens");  
    Log.Checkpoint("Successfully clicked on  Specimens")
}

function using_ControlUp(){
   var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);
    _commonFunctions.actionControlUp(table,"Specimen Table")
    Log.Checkpoint("Successfully clicked Control + Up arrow to select specimen");
}
