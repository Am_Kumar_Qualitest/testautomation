﻿//USEUNIT _commonFunctions
//USEUNIT _config
//USEUNIT quit_Browser

var browser;
var path_txtAccession = "Textbox(\"input_51\")";
var path_SearchButton="Button(\"btn_51\")";
var path_Test="Link(\"a_16_18\")";
var path_ReportingPage = "Page(\"*nj1rptdevappd01.bioreference-laboratories.com*\")";
var path_PanelTest = "Panel(\"tableTests\")";
var path_TableTest = "Table(0)";
var path_TableResults = "Panel(\"tableResults\")";
var path_PatientName = "Textbox(\"input_126\")";
var path_PanelResults = "Panel(\"tableResults\")";
var path_ReportButton = "Button(\"control_580\")";
var path_ViewButton = "Button(\"control_49\")";
var path_PDFContents = "Window(\"AVL_AVView\", \"AVPageView\", 5)"
var path_ResultMessage = "Panel(\"tableResultsText\")";
var path_TestEvent = "Panel(\"tableEvents\")";

function retrive_ResultByAccessionID(accessionID,value){
    
  aqUtils.Delay(5000);
  _commonFunctions.setText(getElement("Name",path_txtAccession),accessionID,"AccessionID");
  getElement("Name",path_SearchButton).Click();
  aqUtils.Delay(5000);
  getElement("contentText",value).Click();  

}

function verify_TestReportingUI(){

    aqUtils.Delay(2000);
  getElement("Name",path_Test).Click();
  aqUtils.Delay(2000);
  if(getElement("contentText","0135-4").Exists){
    Log.Message("Successfully verify in ReportingUI");
  }
  else{
    Log.Message("Failled verify in ReportingUI")
  }  
}

function getElement(prop,value){
  
  if(_config.browser == btIExplorer){
      
      browser = Sys.Browser("iexplore");
  }

    var element = browser.Find([prop,"VisibleOnScreen"],[value,"true"],50);
    if(element.Exists){
      Log.Message("Element located in the \"Search Page\"")
    }else{
      Log.Error("Element does not exist in the \"Search Page\"")
    }
    
    return element;
}

function click_tabTest(){
  aqUtils.Delay(1000);
  getElement("idStr","btn_3_35");
  _commonFunctions.click(getElement("idStr","btn_3_35"),"Test Tab");
}

function click_tabResults(){
  aqUtils.Delay(1000);
  getElement("idStr","btn_4_35");
_commonFunctions.click(getElement("idStr","btn_4_35"),"Results Tab");
}

function getTestData(){
  aqUtils.Delay(1000);
  var panel = getElement("Name","Panel(\"tabTests\")");
  var table = getElement("Name","Table(\"*\")");
  return (aqObject.GetPropertyValue(panel, "innerText"));
}

function getTestDataFromEvents(){
  aqUtils.Delay(1000);
  var panel = getElement("Name","Panel(\"tableEvents\")");
  var table = getElement("Name","Table(\"*\")");
  return (aqObject.GetPropertyValue(panel, "innerText"));
}

function getResultData(){
  aqUtils.Delay(1000);
  var panel = getElement("Name","Panel(\"tableResults\")");
  var table = getElement("Name","Table(\"*\")");
  return (aqObject.GetPropertyValue(panel, "innerText"));
}


function getResultMessageData(){
  aqUtils.Delay(1000);
  var panel = getElement("Name","Panel(\"tableResultsText\")");
  var table = getElement("Name","Table(\"*\")");
  return (aqObject.GetPropertyValue(panel, "innerText"));
}

function getRowID(table,testCode){
  
var i;
    for(i=0; i<table.RowCount; i++){
    
      let cell = table.Cell(i,0);
      
      if(aqString.Trim(cell.innerText) == testCode){
        break;}
      
    }
    return i;
}
function columnID(table, columnHeader){
  
    var i;
    for(i=0; i<table.ColumnCount(i); i++){
    
      let cell = table.Cell(0,i);
      
      if(aqString.Trim(cell.innerText) == columnHeader){
        break;}
      
     }

     return i;
}

function getDataFromTestTab(testCode, columnHeader){
browser = Sys.Browser("iexplore");
var panel = browser.Find(["Name","VisibleOnScreen"],[path_PanelTest,"true"],30);
var table = panel.Find(["Name","VisibleOnScreen"],[path_TableTest,"true"],10);

var cell = table.Cell(getRowID(table,testCode),columnID(table, columnHeader));

return aqString.Trim(cell.innerText);
}

function getDataFromResultTab(testCode, columnHeader){
browser = Sys.Browser("iexplore");
var panel = browser.Find(["Name","VisibleOnScreen"],[path_TableResults,"true"],30);

var table = panel.Find(["Name","VisibleOnScreen"],[path_TableTest,"true"],10);

var cell = table.Cell(getRowID(table,testCode),columnID(table, columnHeader));

return aqString.Trim(cell.innerText);
}

function getPatientName(){
  aqUtils.Delay(3000);
  var name = getElement("Name",path_PatientName);
  return aqObject.GetPropertyValue(name, "Text");
}

function click_ReportsButton(){
    
  aqUtils.Delay(5000);
  browser = Sys.Browser("iexplore");
  var reportButton = browser.Find("Name",path_ReportButton,40);
  _commonFunctions.click(reportButton,"Report Button");

}

function click_ViewButton(){
    
  aqUtils.Delay(10000);
  Sys.Browser("iexplore").BrowserWindow(2).Maximize()
  browser = Sys.Browser("iexplore");
  var viewButton = browser.Find("Name",path_ViewButton,50);
  _commonFunctions.click(viewButton,"View Button");
  
  aqUtils.Delay(10000);
  Sys.Browser("iexplore").BrowserWindow(3).Maximize();

}

function getDataFromResultMessage(){
browser = Sys.Browser("iexplore");
var panel = browser.Find(["Name","VisibleOnScreen"],[path_ResultMessage,"true"],40);

var table = panel.Find(["Name","VisibleOnScreen"],[path_TableTest,"true"],10);

//var cell = table.Cell(getRowID(table,testCode),columnID(table, columnHeader));

return aqString.Trim(table.innerText);
}

function getDataTestEvent(testCode, columnHeader){
browser = Sys.Browser("iexplore");
var panel = browser.Find(["Name","VisibleOnScreen"],[path_TestEvent,"true"],40);

var table = panel.Find(["Name","VisibleOnScreen"],[path_TableTest,"true"],2);

var cell = table.Cell(getRowID(table,testCode),columnID(table, columnHeader));

return aqString.Trim(cell.innerText);
}

function verifyListofTestEventData(){
  
    
}
