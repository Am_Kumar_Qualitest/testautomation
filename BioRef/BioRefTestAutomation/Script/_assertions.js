﻿//USEUNIT _commonFunctions

function CheckEquals(aValue1, aValue2) {

  Log.Message("Validating "+aValue1 + " and "+ aValue2 +" are matching to each other");

  if (aqString.Compare(aValue1, aValue2,true) != 0)

    Log.Error("'" + aValue1 + "' does NOT match value '" + aValue2+"'");

  else

    Log.Checkpoint("'" + aValue1 + " matches value '"+ aValue2+ "'");

}

function CheckContains(aValue1, aValue2) {

  Log.Message("Validating "+aValue1 + " contains the string "+ aValue2 +" in it");

  if (aqString.Contains(aValue1, aValue2) == -1)

    Log.Error("'" + aValue1 + "' does NOT contains value '" + aValue2+"'");

  else

    Log.Checkpoint("'" + aValue1 + " contains value '"+ aValue2+ "'");

}

function CheckNotContains(aValue1, aValue2) {

  Log.Message("Validating "+aValue1 + " not contains the string "+ aValue2 +" in it");

  if (aqString.Contains(aValue1, aValue2) != -1)

    Log.Error("'" + aValue1 + "' contains value '" + aValue2+"'");

  else

    Log.Checkpoint("'" + aValue1 + " does not contains value '"+ aValue2+ "'");

}

function checkArrayContains(arr, value){

var flag;
  
  for (var i=0;i<arr.length;i++){

    if(aqString.Contains(arr[i],value) != -1){
      
    flag = true;
    break;
    }else{
      flag = false;
    }
  }
  
  if (flag == true)

    Log.Checkpoint("Array contains the value \"" + value + "\" in it");

  else

    Log.Error("Array does not contains the value \"" + value + "\" in it");
}

function checkArrayNotContains(arr, value){

var flag;
  
  for (var i=0;i<arr.length;i++){

    if(aqString.Contains(arr[i],value) != -1){
      
    flag = false;
    break;
    }else{
      flag = true;
    }
  }
  
  if (flag == true)

    Log.Checkpoint("Array does not contains the value \"" + value + "\" in it");

  else

    Log.Error("Array contains the value \"" + value + "\" in it");
}

function assertExists(element){
  
    var process_SPM = Sys.Process("SPM");
    var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
    var txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    var path_textboxLabel = "WinFormsObject(\"lblItem\")";

    var elementPath = process_SPM.Find(["Name","WndCaption","VisibleOnScreen"],[path_textboxLabel,element,"true"],10);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Additional Information Page\"")
    }else{
      Log.Error("Element does not exist in the \"Additional Information Page\"")
    }
}

function assertEmpty(value){
  
  Log.Message("Validating "+value + " is empty or not");

  if (aqString.GetLength(value) != 0)

    Log.Error(value+ " is not empty");

  else

    Log.Checkpoint("Provided text value is empty");
  
}

function verifyTestCodes(logMessage,arrayData,logName){
  Log.Message("Verifying the test code value in : "+logName);
  for(var i=0; i<arrayData.length; i++){
    _assertions.CheckContains(logMessage,arrayData[i]);
  } 
}

function verifyTestCodeandActionType(logMessage,arrayData,actionType,logName,){
  Log.Message("Verifying the test code value in : "+logName);
  for(var i=0; i<arrayData.length; i++){
    _assertions.CheckEquals(_commonFunctions.getJSONData(logMessage,arrayData[i]),actionType);
  } 
}

