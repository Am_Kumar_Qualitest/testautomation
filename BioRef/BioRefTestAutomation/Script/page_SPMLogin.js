﻿//USEUNIT quit_Browser
//USEUNIT _commonFunctions
var process_SPM, login_form;
var path_txtBoxUserName = "WinFormsObject(\"txtUserName\")";
var path_txtBoxPassword = "WinFormsObject(\"txtPassword\")";
var path_btnLogin = "WinFormsObject(\"btnLogin\")";
var path_dropDownDivison = "WinFormsObject(\"cbxPromptList\")";
var path_btnOk = "WinFormsObject(\"btnOK\")";
var path_popupDivison = "frmPromptSelect";
var path_popupLogin = "WinFormsObject(\"frmLogin\")";

/****************************************************/
//Function Name: enterval_UserName
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to entering text in password field
//Parameters: User Name
/****************************************************/
function enterval_UserName(UserName){
 

  Sys.WaitProcess("SPM",5000);
  process_SPM = Sys.Process("SPM");
  process_SPM.WaitWinFormsObject(path_popupLogin,5000);  
  txtBoxUserName = process_SPM.Find("Name",path_txtBoxUserName,10);
  
  _commonFunctions.setText(txtBoxUserName,UserName,"UserName");

}

/****************************************************/
//Function Name: enterval_Password
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to entering text in password field
//Parameters: password
/****************************************************/
function enterval_Password(Password){
 
  txtBoxPassword = process_SPM.Find("Name",path_txtBoxPassword,10);
  
  _commonFunctions.setText(txtBoxPassword,Password,"Password");

}

/****************************************************/
//Function Name: click_LoginButton
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to clicking on LoginButton
//Parameters: 
/****************************************************/
function click_LoginButton(){

  btnLogin = process_SPM.Find("Name",path_btnLogin,10);
  
  _commonFunctions.click(btnLogin,"Login button");

}

/****************************************************/
//Function Name: select_Division
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to selecting a division
//Parameters: DivisonName
/****************************************************/
function select_Division(DivisonName){

    process_SPM.WaitWinFormsObject(path_popupDivison,10000);
    
    var dropDownDivison = process_SPM.Find("Name", path_dropDownDivison,10);
    
    _commonFunctions.clickItem(dropDownDivison,DivisonName);

}
/****************************************************/
//Function Name: select_Division
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to clicking on OK button
//Parameters: 
/****************************************************/
function click_OkButton(){

    var btnOK = process_SPM.Find("Name", path_btnOk,10);
   
    _commonFunctions.click(btnOK,"Ok button");

}