﻿//USEUNIT _commonFunctions
//USEUNIT _config
//USEUNIT quit_Browser

var process_SPM, frm_beginOrder, txtBox_SPM, box_MegaSearch;
var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";
box_MegaSearch = "WinFormsObject(\"MegaSearch\")";

/****************************************************/
//Function Name: enterval_SpecimenTrackingID
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in SpecimenTrackingID field
//Parameters: 
/****************************************************/
function enterval_SpecimenTrackingID(value){  
    
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_textboxGeneric,10);
    searchBox = process_SPM.Find("Name",box_MegaSearch,10);

    assertExists("*Specimen Tracking Id"); 
    _commonFunctions.setText(txtBox_SPM,value,"SpecimenTrackingID");
    _commonFunctions.actionTab(txtBox_SPM,"SpecimenTrackingID");

}
/****************************************************/
//Function Name: enterval_BioRequisition
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in BioRequisition field
//Parameters: 
/****************************************************/
function enterval_BioRequisition(value){
   
    assertExists("*Bio Requisition:"); 
    _commonFunctions.setText(txtBox_SPM,value,"BioRequisition");
    _commonFunctions.actionTab(txtBox_SPM,"BioRequisition");
}
/****************************************************/
//Function Name: enterval_NamesMatch
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in NamesMatch field
//Parameters: 
/****************************************************/
function enterval_NamesMatch(value){

    assertExists("*Names Match:");
    _commonFunctions.setText(txtBox_SPM,value,"NamesMatch");
    _commonFunctions.actionTab(txtBox_SPM,"NamesMatch");

}
/****************************************************/
//Function Name: enterval_TwoIDsOnSpecimen
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in TwoIDsOnSpecimen field
//Parameters: 
/****************************************************/
function enterval_TwoIDsOnSpecimen(value){
 
    assertExists("*Two Identifiers on Specimen*");
    _commonFunctions.setText(txtBox_SPM,value,"TwoIDsOnSpecimen");
    _commonFunctions.actionTab(txtBox_SPM,"TwoIDsOnSpecimen");
}

/****************************************************/
//Function Name: enterval_Account
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in Account field
//Parameters: 
/****************************************************/
function enterval_Account(value){
  
    assertExists("*Account*");
    _commonFunctions.setText(txtBox_SPM,value,"Account");
    _commonFunctions.actionTab(txtBox_SPM,"Account");

    
    if(value == "G7751" || value == "SG001" || value == "M8362"){
      _commonFunctions.actionShiftTab(txtBox_SPM,"Account");
    }
}
/****************************************************/
//Function Name: enterval_OrderType
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in OrderType field
//Parameters: 
/****************************************************/
function enterval_OrderType(value){
  
    assertExists("*Order Type:");
    _commonFunctions.setText(txtBox_SPM,value,"OrderType");
    process_SPM.WaitWindow("#32770", "Client Master Registration Type", 5000)
    _commonFunctions.actionTab(txtBox_SPM,"OrderType");
 
    if(process_SPM.Find("WndCaption", "Client Master Registration Type", 3).Exists){
      var clientMasterResgistrationPopup = process_SPM.Find("WndCaption", "Client Master Registration Type", 3);
      var btn_Yes = clientMasterResgistrationPopup.Window("Button", "&Yes", 1);
    
    if(btn_Yes.Exists){  
      _commonFunctions.click(btn_Yes,"Yes button")
    }
    }
  
}
/****************************************************/
//Function Name:enterval_SpecimenCount
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in SpecimenCoun field
//Parameters: 
/****************************************************/
function enterval_SpecimenCount(value){
  
  
    assertExists("*Specimen Count:");
    _commonFunctions.setText(txtBox_SPM,value,"SpecimenCount");
    _commonFunctions.actionTab(txtBox_SPM,"SpecimenCount");
}
/****************************************************/
//Function Name:enterval_TestCount
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in TestCount field
//Parameters: 
/****************************************************/
function enterval_TestCount(value){

    assertExists("*Test Count:");
    _commonFunctions.setText(txtBox_SPM,value,"TestCount");
    _commonFunctions.actionTab(txtBox_SPM,"TestCount");

}
/****************************************************/
//Function Name:popup_accountConfirmation
//Created By: Yogeswaran B 
//Created on: 
//Description: This function helps to Entering value in accountConfirmation field
//Parameters: valueCheckDigit, valueAccNumbe
/****************************************************/
function popup_accountConfirmation(valueCheckDigit, valueAccNumber){
  
    var popup_accConfirmation = process_SPM.WinFormsObject("FrmPromptAccountConfirmation").WinFormsObject("Panel1");
    
    var txt_CheckDigit = popup_accConfirmation.WinFormsObject("txtCheckDigit");
    var txt_AccountNumber = popup_accConfirmation.WinFormsObject("txtAccountNumber");
   
    _commonFunctions.setText(txt_CheckDigit,valueCheckDigit,"CheckDigit");
    _commonFunctions.actionTab(txt_CheckDigit,"CheckDigit");

    _commonFunctions.setText(txt_AccountNumber,valueAccNumber,"AccountNumber");
    _commonFunctions.actionEnter(txt_AccountNumber,"AccountNumber");

}
/****************************************************/
//Function Name:verify_ErrorMessageForSpecimenTrackingID
//Created By: Raja Singh 
//Created on: 
//Description: This function helps to validating error message
//Parameters: 
/****************************************************/
function verify_ErrorMessageForSpecimenTrackingID(expectedErrorMessage)
{
    process_SPM = Sys.Process("SPM");
    beginOrder = process_SPM.WinFormsObject("frmSPM").WinFormsObject("TableLayoutPanel1").WinFormsObject("panelEntry").WinFormsObject("TableLayoutPanel4");
    txtBox_SPM = beginOrder.WinFormsObject("txtItem");
    _commonFunctions.setText(txtBox_SPM,_config.invalidSpecimenTrackingID);
    _commonFunctions.actionTab(txtBox_SPM,"SpecimenTrackingID");
    error =process_SPM.WinFormsObject("frmSPM").WinFormsObject("TableLayoutPanel1").WinFormsObject("panelEntry").WinFormsObject("lblInfo");
    actualErrorMessage=error.Text;
    if(aqString.Compare(expectedErrorMessage,actualErrorMessage,true)){
    Log.Message("Successfully matched Actual error message to Expected error message and Actual error message is   "+actualErrorMessage);
    }
    else{
      Log.Error("Not Found Error Message");
    }
    _commonFunctions.setText(txtBox_SPM,"");
}
/****************************************************/
//Function Name:verify_AllSpecimenTrackinID
//Created By: Raja Singh 
//Created on: 
//Description: This function helps to validating allSpecimenTrackinID by using ctrl+[UP]
//Parameters: 
/****************************************************/
function verify_AllSpecimenTrackinID(){
  _commonFunctions.actionControlUp(txtBox_SPM,"SpecimenTrackingID");
  _commonFunctions.actionControlUp(txtBox_SPM,"SpecimenTrackingID");
  _commonFunctions.actionControlUp(txtBox_SPM,"SpecimenTrackingID");
  Log.Message("Successfully checked All Three SpecimenTrackingID is availble")
}

function clicking_OnBeginOrder(){
 

    var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);//.get_Properties("wTabCount");
    var tabControl = table.TabControl;
    tabControl.ClickTab("Begin Order");
    Log.Checkpoint("Successfully clicked on Begin Order")
}

function assertExists(element){
    process_SPM.WaitWinFormsObject(path_textboxGeneric,1000);
    var elementPath = process_SPM.Find(["Name","VisibleOnScreen"],[path_textboxLabel,"true"],20);
    elementPath.WaitProperty("WndCaption",element,2000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Begin Order Page\"")
    }else{
      Log.Error("Element does not exist in the \"Begin Order Page\"")
    }
}

