﻿//USEUNIT _commonFunctions


var process_SPM, textBox_Generic;
var path_Accession="WinFormsObject(\"txtSrcAccessionNo\")";
var path_SearchButton="WinFormsObject(\"btnSearch\")";
var path_SearchResult="WinFormsObject(\"dgvSearchResult\")";
var path_textboxGeneric = "WinFormsObject(\"txtItem\")";

function get_ResultByAccession(value){
  
  process_SPM = Sys.Process("SPM");
  textbox_Accession = process_SPM.Find("Name",path_Accession,10);
  searchButton =process_SPM.Find("Name",path_SearchButton,10);
  _commonFunctions.setText(textbox_Accession,value,"Accession");
    _commonFunctions.actionTab(textbox_Accession,"Accession");
    searchButton.Click();
  }
  
function click_OnSearchResult()
{
  process_SPM = Sys.Process("SPM");
  searchResult= process_SPM.Find("Name",path_SearchResult,10);
  searchResult.DblClick();
}

function unlockAccession(){
  
   process_SPM = Sys.Process("SPM");
   textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
  _commonFunctions.actionCtrlU(textboxGeneric,"Unlock Accession") ;
}