﻿//USEUNIT _commonFunctions
//USEUNIT quit_Browser

var process_SPM, addInformation, txtBox_SPM;
var path_GenericTextBox = "WinFormsObject(\"txtItem\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";

/****************************************************/
//Function Name: enterval_ReferringPhysician
//Created By: Raja Singh
//Created on: 
//Description: This function helps to Entering value in ReferringPhysician field
//Parameters: 
/****************************************************/
function enterval_ReferringPhysician(value){

    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_GenericTextBox,10);
    assertExists("*Referring Physician:");
    _commonFunctions.setText(txtBox_SPM,value,"ReferringPhysician");
    _commonFunctions.actionDoubleTab(txtBox_SPM,"ReferringPhysician");
}
/****************************************************/
//Function Name: enterval_PleaseEnterRequisition
//Created By: Raja Singh
//Created on: 
//Description: This function helps to Entering value in PleaseEnterRequisition field
//Parameters: 
/****************************************************/
function enterval_Requisition(value){
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_GenericTextBox,10);
    assertExists("*Please enter Requisition*");
    _commonFunctions.setText(txtBox_SPM,value,"Requisition");
    _commonFunctions.actionTab(txtBox_SPM,"Requisition");

}//HISPANIC
function enterval_Race(value){
  
    assertExists("*Race:");
    _commonFunctions.setText(txtBox_SPM,value,"Race");
    _commonFunctions.actionTab(txtBox_SPM,"Race");
}
function enterval_Ethnicity(value){
  

    assertExists("*Ethnicity:");
    _commonFunctions.setText(txtBox_SPM,value,"Source");
    _commonFunctions.actionTab(txtBox_SPM,"Source");
}

function enterval_Source(value){
  
	  assertExists("*Source:*");
    _commonFunctions.setText(txtBox_SPM,value,"Source");
    _commonFunctions.actionTab(txtBox_SPM,"Source");

}

function enterval_2ndSampleReceived(value){
  

	  assertExists("*Was the 2nd sample received*");
    _commonFunctions.setText(txtBox_SPM,value,"2ndSampleReceivedOrNot");
    _commonFunctions.actionTab(txtBox_SPM,"2ndSampleReceivedOrNot");
}

function enterval_3rdSampleReceived(value){

    assertExists("*Was the 3rd sample received*");
    _commonFunctions.setText(txtBox_SPM,value,"3rdSampleReceivedOrNot");
    _commonFunctions.actionTab(txtBox_SPM,"3rdSampleReceivedOrNot");
}

function enterval_PatientRoom(value){

    assertExists("*Patient Room*");
    _commonFunctions.setText(txtBox_SPM,value,"Patient Room");
    _commonFunctions.actionTab(txtBox_SPM,"Patient Room");
}

function enterval_PatientBed(value){

    assertExists("*Patient Bed*");
    _commonFunctions.setText(txtBox_SPM,value,"Patient Bed");
    _commonFunctions.actionTab(txtBox_SPM,"Patient Bed");
}

function enterval_PatientFloor(value){

    assertExists("*Patient Floor*");
    _commonFunctions.setText(txtBox_SPM,value,"Patient Floor");
    _commonFunctions.actionTab(txtBox_SPM,"Patient Floor");
}

function assertExists(element){
    process_SPM.WaitWinFormsObject(path_GenericTextBox,1000);
    var elementPath = process_SPM.Find(["Name","WndCaption","VisibleOnScreen"],[path_textboxLabel,element,"true"],10);
    elementPath.WaitProperty("WndCaption",element,2000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Additional Information Page\"")
    }else{
      Log.Error("Element does not exist in the \"Additional Information Page\"")
    }
}
function enterval_ChooseCaseType(value){
  

	  assertExists("*Choose Case Type:");
    _commonFunctions.setText(txtBox_SPM,value,"Choose Case Type");
    _commonFunctions.actionTab(txtBox_SPM,"Choose Case Type");
}
function enterval_TestSpecificQuestion(value){
  

	  assertExists("*4000 BIOPSY, GENERAL - Source::");
    _commonFunctions.setText(txtBox_SPM,value,"4000 BIOPSY, GENERAL - Source");
    _commonFunctions.actionTab(txtBox_SPM,"4000 BIOPSY, GENERAL - Source");
}

function enterval_0082_source(value){  

    assertExists("*0082*"); 
    _commonFunctions.setText(txtBox_SPM,value,"0082_Source");
    _commonFunctions.actionTab(txtBox_SPM,"0082_Source");

}

function enterval_0182_source(value){  

    assertExists("*0182*"); 
    _commonFunctions.setText(txtBox_SPM,value,"0182_Source");
    _commonFunctions.actionTab(txtBox_SPM,"0182_Source");

}

function enterval_0828_source(value){  

    assertExists("*0828*"); 
    _commonFunctions.setText(txtBox_SPM,value,"0828_Source");
    _commonFunctions.actionTab(txtBox_SPM,"0828_Source");

}

function enterval_0556_source(value){  

    assertExists("*0556*"); 
    _commonFunctions.setText(txtBox_SPM,value,"0556_Source");
    _commonFunctions.actionTab(txtBox_SPM,"0556_Source");

}
function enterval_IsPatientSignature(value){

    assertExists("*Is patient signature, name and date present on the requisition?  (all three required for yes):");
    _commonFunctions.setText(txtBox_SPM,value,"Question1");
    _commonFunctions.actionTab(txtBox_SPM,"Question1");

}
function enterval_IsProviderSignature(value){

    assertExists("*Is provider signature, name and date present on the requisition? (all three required for yes):");
    _commonFunctions.setText(txtBox_SPM,value,"Question2");
    _commonFunctions.actionTab(txtBox_SPM,"Question2");

}
function enterval_PatientConfirmedPSA(value){//3

    assertExists("*Patient confirmed PSA (two or more results several weeks apart):");
    _commonFunctions.setText(txtBox_SPM,value,"Question3");
    _commonFunctions.actionTab(txtBox_SPM,"Question3");

}
function enterval_IsThe4KscoreTestMedically(value){//4

    assertExists("*Is the 4Kscore Test medically reasonable and necessary for the prostate biopsy decision, or does the patient have factors that indicate a biopsy should occur no matter what the 4Kscore result is?:");
    _commonFunctions.setText(txtBox_SPM,value,"Question4");
    _commonFunctions.actionTab(txtBox_SPM,"Question4");

}
function enterval_BelowFactorsHaveBeenTakenIntoConsideration(value){//5

    assertExists("*Below factors have been taken into consideration in making the decision to order the 4Kscore Test: a) some ethnicities are known to have a higher risk for prostate cancer, b) the patient should have at least a 10-year life expectancy, c) the patient has been worked up for benign disease.:");
    _commonFunctions.setText(txtBox_SPM,value,"Question5");
    _commonFunctions.actionTab(txtBox_SPM,"Question5");

}
function enterval_HasThePatientHadAPreviousBiopsy(value){//6

    assertExists("*4Kscore:  Has the patient had a previous biopsy?:");
    _commonFunctions.setText(txtBox_SPM,value,"Question6");
    _commonFunctions.actionTab(txtBox_SPM,"Question6");

}
function enterval_WhatWasThePatientDREResults(value){//7

    assertExists("*4Kscore:  What was the patient's DRE Results?:");
    _commonFunctions.setText(txtBox_SPM,value,"Question7");
    _commonFunctions.actionTab(txtBox_SPM,"Question7");

}
function enterval_PatientHistory(value){//8

    assertExists("*Patient History (Choose all that apply):");
    _commonFunctions.setText(txtBox_SPM,value,"Question8");
    _commonFunctions.actionDoubleTab(txtBox_SPM,"Question8");

}
function enterval_OtherPatientHistory(value){//9

    assertExists("*Other (Patient History)::");
    _commonFunctions.setText(txtBox_SPM,value,"Question9");
    _commonFunctions.actionTab(txtBox_SPM,"Question9");

}
function enterval_4KTestOrderDiscussedWithPatient(value){//10

    assertExists("*4K test order discussed with patient?:");
    _commonFunctions.setText(txtBox_SPM,value,"Question10");
    _commonFunctions.actionTab(txtBox_SPM,"Question10");

}

function enterval_IsPatientPregnant(value){
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_GenericTextBox,10);
    assertExists("*IS PATIENT PREGNANT*");
    _commonFunctions.setText(txtBox_SPM,value,"PatientPregnant");
    _commonFunctions.actionTab(txtBox_SPM,"PatientPregnant");

}

function clicking_OnAddInfo(){
 
    var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);
    var tabControl = table.TabControl;
    tabControl.ClickTab(" Add'l Information ");//  Review Order  
    Log.Checkpoint("Successfully clicked on  Add'l Information ")
}
function performTapActionOnAddInfoPage(){
  process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_GenericTextBox,10);
     _commonFunctions.actionTab(txtBox_SPM,"Performed Tab Action");
}

function enterval_HasConsentFormBeenReceived(value){
  

    assertExists("*HAS CONSENT FORM BEEN RECEIVED?:");
    _commonFunctions.setText(txtBox_SPM,value,"HAS CONSENT FORM BEEN RECEIVED");
    _commonFunctions.actionTab(txtBox_SPM,"HAS CONSENT FORM BEEN RECEIVED");
}
function PerformTabAction(NoOfTab)
{
    process_SPM = Sys.Process("SPM");
    txtBox_SPM = process_SPM.Find("Name",path_GenericTextBox,10);
    for(var i =0;i<=NoOfTab;i++){
    _commonFunctions.actionTab(txtBox_SPM,"");
    }
  
}

function enterval_IndicationsForTesting(value){

    assertExists("*Indications For Testing*");
    _commonFunctions.setText(txtBox_SPM,value,"Indications For Testing");
    _commonFunctions.actionTab(txtBox_SPM,"Indications For Testing");
    _commonFunctions.actionTab(txtBox_SPM,"Indications For Testing");
}

function enterval_ConcentFormReceived(value){

    assertExists("*Consent Form Been Received?:");
    _commonFunctions.setText(txtBox_SPM,value,"Consent Form Been Received?");
    _commonFunctions.actionTab(txtBox_SPM,"Consent Form Been Received?");
}
