﻿//USEUNIT _commonFunctions
//USEUNIT _config

var browser, browserIndex;
path_txtUserName = "Textbox(\"Login1_txtUsername\")";
path_txtPassword = "PasswordBox(\"Login1_txtPassword\")";
path_txtAccessionNumber = "Textbox(\"Login1_txtAccessionNumber\")";
path_btnLogin = "SubmitButton(\"Login1_btnLogin\")";

function enterval_UserName(username){
  
    browserIndex = Sys.Browser("iexplore").BrowserIndex;
    Sys.Browser("iexplore").BrowserWindow(browserIndex).Maximize();
   _commonFunctions.setText(getElement("Name",path_txtUserName),username,"UserName");
}

function enterval_Password(password){
  
   _commonFunctions.setText(getElement("Name",path_txtPassword),password,"Password");
}

function enterval_AccessionID(accessionNumber){
  
   _commonFunctions.setText(getElement("Name",path_txtAccessionNumber),accessionNumber,"Accession Number");
}

function clickLoginBtn(){
  
   _commonFunctions.click(getElement("Name",path_btnLogin),"Login Button");
}

function getElement(prop,value){
  
  if(_config.browser == btIExplorer){
      
      browser = Sys.Browser("iexplore");
  }

    var element = browser.Find([prop,"VisibleOnScreen"],[value,"true"],20);
    
    for(var i=0;i<10;i++){
    if(!(browser.Find([prop],[value],20)).VisibleOnScreen){
    aqUtils.Delay(1000);}
    else{
      break;
    }
    }
        
    if(element.Exists){
      Log.Message("Element located in the \"Login Page\"")
    }else{
      Log.Error("Element does not exist in the \"Login Page\"")
    }
    
    return element;
}