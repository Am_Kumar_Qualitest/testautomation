﻿//USEUNIT _commonFunctions

var process_TMA;
var path_textBoxSearch="WinFormsObject(\"utxtSearch\")";
var path_embededSearch = "WinFormsObject(\"utxtSearch_EmbeddableTextBox\")";
var path_resultGrid = "WinFormsObject(\"ugrdSearchResult\")";

function search_TestCode(value){
  
  process_TMA = Sys.Process("TestMaster");
  process_TMA.WaitWinFormsObject("WinFormsObject(\"frmTestSearch\")",3000);
  searchTest = process_TMA.Find("Name",path_embededSearch,10);
  
  _commonFunctions.setText(searchTest,value,"Test Code");
}

function searchAndSelectfrmResultGrid(columnName,value){
    
    resultGrid = process_TMA.Find("Name",path_resultGrid,10);
    
    for(var i =0;i<10;i++){
    if(resultGrid.Rows.Count>0){
      break;
    }else{
      aqUtils.Delay(1000);
    }
    }
    
    _commonFunctions.doubleClk_RowFromDataGrid(resultGrid,columnName,value);
    Log.Message("Sucessfully clicked on the row with value "+value);
}

function close_searchTestCodePopup(){
  
  var searchTestCodePopup = process_TMA.Find("Name","WinFormsObject(\"frmTestSearch\")",10);
  searchTestCodePopup.Close();
  Log.Message("Sucessfully Search Test Code Popup closed");
  
}