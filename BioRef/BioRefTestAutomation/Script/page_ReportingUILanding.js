﻿//USEUNIT _commonFunctions
//USEUNIT _config

var browser;
path_Result = "Link(\"a_5_18\")";

function clickOnResult()
{
  aqUtils.Delay(5000);
  getElement("contentText","Results").Click();
}
function getElement(prop,value){
  
  if(_config.browser == btIExplorer){
      
      browser = Sys.Browser("iexplore");
  }
    for(var i =0;i<60;i++){  
  
    var element = browser.Find([prop],[value],50);
    if(!element.VisibleOnScreen){
      //Log.Error("Element does not exist in the \"Landing Page\"")
      aqUtils.Delay(1000);
    }else{
      Log.Message("Element located in the \"Landing Page\"");
      break;
    }
    }
    return element;
}