﻿//USEUNIT _commonFunctions
//USEUNIT _assertions
//USEUNIT quit_Browser

var process_SPM, reviewOrder,textBox_Generic;
var popupSummary, popUpOKBtn, dgvReviewSpecimen, scrollableTable;

var path_AccessionPopup = "Window(\"Static\", \"Order has been saved successfully*\", *)";
var path_AccessionPopupBtnOK = "Window(\"Button\", \"OK\", *)";
var path_textboxGeneric = "WinFormsObject(\"txtItem\")";
var path_PhyscianName = "WinFormsObject(\"lblReviewPhys\")";
var path_Priority = "WinFormsObject(\"lblReviewPriority\")";
var path_PaitentID = "WinFormsObject(\"lblReviewPatID\")";
var path_Gender = "WinFormsObject(\"lblReviewGender\")";
var path_DrawDate = "WinFormsObject(\"lblReviewDrawDate\")";
var path_DateofBirth = "WinFormsObject(\"lblReviewDOB\")";
var path_Fasting = "WinFormsObject(\"lblReviewFasting\")";
var path_UserName = "WinFormsObject(\"lblUsername\")";
var path_TestCount = "WinFormsObject(\"lblTestCount\")";
var path_SpecimenCount = "WinFormsObject(\"lblSpecimenCount\")";
var path_ScrollableTest= "WinFormsObject(\"frmScrollableInfo\")";
var path_ReviewScrollableTest= "WinFormsObject(\"dgvReviewTests\")";
var path_ReviewSpecimens= "WinFormsObject(\"dgvReviewSpecimens\")";
var path_SpecimensRequestedLabel="WinFormsObject(\"txtMsg\")";
var path_SpecimensRequestedLabelOkButton="WinFormsObject(\"btnOK\")";
var path_txtOtherReason="WinFormsObject(\"txtOtherReason\")";
var path_UnlockMessage="WinFormsObject(\"lblInfo\")"
var path_OkButton="WinFormsObject(\"btnOK\")";
var path_textboxLabel = "WinFormsObject(\"lblItem\")";
var path_dgvReviewTests =  "WinFormsObject(\"dgvReviewTests\")";
var path_ScrollableWindow = "WinFormsObject(\"frmScrollableInfo\")";
var path_dgvReviewSpecimen = "WinFormsObject(\"dgvReviewSpecimens\")";
var path_AlertPopup = "WinFormsObject(\"frmMsgBox\")";
var path_AlertMessage = "WinFormsObject(\"txtMsg\")";
var path_btnAlertOk = "WinFormsObject(\"btnOK\")";
var path_LastName = "WinFormsObject(\"lblReviewPatLName\")";
var path_FirstName = "WinFormsObject(\"lblReviewPatFName\")";
var path_popupSummary = "WinFormsObject(\"txtMsg\")";
var path_popUpOKBtn = "WinFormsObject(\"btnOK\")";
var path_scrollableTable = "WinFormsObject(\"frmScrollableInfo\")";
var path_dgvReviewTests =  "WinFormsObject(\"dgvReviewTests\")";

/****************************************************/
//Function Name: enterval_Account
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to entering value in Account field
//Parameters: value
/****************************************************/
function enterval_Account(value){
  
    process_SPM = Sys.Process("SPM");
    textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
    assertExists("*Account*");
    _commonFunctions.setText(textBox_Generic,value,"Account");
    _commonFunctions.actionTab(textBox_Generic,"Account");
}
/****************************************************/
//Function Name: enterval_LastName
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to entering value in LastName field
//Parameters: value
/****************************************************/

function enterval_LastName(value){
  
    assertExists("*Last Name:");
    _commonFunctions.setText(textBox_Generic,value,"LastName");
    _commonFunctions.actionTab(textBox_Generic,"LastName");
}
/****************************************************/
//Function Name: enterval_FirstName
//Created By: Yogeswaran B

//Created on: 
//Description: This function helps to entering value in FirstName field
//Parameters: value
/****************************************************/
function enterval_FirstName(value){
  
    assertExists("*First Name:");
    _commonFunctions.setText(textBox_Generic,value,"FirstName");
    _commonFunctions.actionTab(textBox_Generic,"FirstName");
}
/****************************************************/
//Function Name: enterval_Submit
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to entering value in Submit field
//Parameters: value
/****************************************************/
function enterval_Submit(value){
  
    assertExists("*Submit*");
    textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
    _commonFunctions.setText(textBox_Generic,value,"Submit");
    _commonFunctions.actionTab(textBox_Generic,"Submit");
}

function enterval_PatientID(value){
  
    assertExists("*Patient ID:");
    textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
    _commonFunctions.setText(textBox_Generic,value,"Patient ID");
    _commonFunctions.actionTab(textBox_Generic,"Patient ID");
}
function enterval_DateOfBirth(value){
  
    //assertExists("*DOB/Age:");
    textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
    _commonFunctions.setText(textBox_Generic,value,"DOB/Age");
    _commonFunctions.actionTab(textBox_Generic,"DOB/Age");
}

function setSubmit(){
 
  process_SPM = Sys.Process("SPM");
  textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
  //_commonFunctions.setText(textBox_Generic,"T","Submit ");
 
  textBox_Generic.Keys("T");
 
}

function performTabSubmit(){

    _commonFunctions.actionTab(textBox_Generic,"Submit");
}
/****************************************************/
//Function Name: enterval_Account
//Created By: Yogeswaran B

//Created on: 
//Description: This function helps to getting AccessionID
//Parameters: 
/****************************************************/
function getAccessionID(){
  
    var subString, AccessionID;
    process_SPM.WaitWinFormsObject(path_AccessionPopup,3000);
       
    let popup = process_SPM.Find("Name",path_AccessionPopup,10);
    
    if(popup.Exists){
      
    let btnOK = process_SPM.Find("Name",path_AccessionPopupBtnOK,10);
     
     subString = _commonFunctions.getStringValue(popup.Name,"Accession",25);
     
     AccessionID = aqString.SubString(subString, 12, 9);
     
     _commonFunctions.click(btnOK,"Accession popup Ok button");
    }
    
    Log.Message("Successfully retrived AccessionID from the Application: "+AccessionID);
    
    return AccessionID;

}
/****************************************************/
//Function Name: getPhyscianName
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting PhysicianName
//Parameters: 
/****************************************************/
function getPhyscianName(){
  
    process_SPM = Sys.Process("SPM");
    var PhyscianName = process_SPM.Find("Name",path_PhyscianName,20);
    
    _commonFunctions.getText(PhyscianName,"PhyscianName");
}

function getLastName(){
  
    process_SPM = Sys.Process("SPM");
    var LastName = process_SPM.Find("Name",path_LastName,20);
    
    return _commonFunctions.getValue(LastName,"LastName");
}

function getFirstName(){
  
    process_SPM = Sys.Process("SPM");
    var FirstName = process_SPM.Find("Name",path_FirstName,20);
    
    return _commonFunctions.getValue(FirstName,"FirstName");
}
/****************************************************/
//Function Name: getPriority
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting Priority
//Parameters: 
/****************************************************/
function getPriority(){
  
    var Priority = process_SPM.Find("Name",path_Priority,10);
    
    return _commonFunctions.getValue(Priority,"Priority");
}
/****************************************************/
//Function Name: getPatientID
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting patientID
//Parameters: 
/****************************************************/
function getPatientID(){
  
    var PatientID = process_SPM.Find("Name",path_PaitentID,10);    
    
     return _commonFunctions.getValue(PatientID,"PatientID");
}
/****************************************************/
//Function Name: getGender
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting Gender
//Parameters: 
/****************************************************/
function getGender(){
  
    var Gender = process_SPM.Find("Name",path_Gender,10);
    
    Log.Message("Getting the Gender");
   return _commonFunctions.getValue(Gender,"Gender");
}
/****************************************************/
//Function Name: getDrawDate
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting DrawDate
//Parameters: 
/****************************************************/
function getDrawDate(){
  
    var DrawDate = process_SPM.Find("Name",path_DrawDate,10);
    
    Log.Message("Getting the DrawDate");
    return _commonFunctions.getValue(DrawDate,"DrawDate");
}
/****************************************************/
//Function Name: enterval_Account
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting value from DateOfBirth field
//Parameters: 
/****************************************************/
function getDateOfBirth(){
  
    var DateOfBirth = process_SPM.Find("Name",path_DateofBirth,10);
    
    Log.Message("Getting the DateOfBirth");
    return _commonFunctions.getValue(DateOfBirth,"DoB");
}
/****************************************************/
//Function Name: getFasting
//Created By:Yogeswaran B

//Created on: 
//Description: This function helps to getting 
//Parameters: 
/****************************************************/
function getFasting(){
  
    var Fasting = process_SPM.Find("Name",path_Fasting,10);
    
    Log.Message("Getting the Fasting");
    return _commonFunctions.getValue(Fasting,"Fasting");
}
/****************************************************/
//Function Name: getUserName
//Created By: Yogeswaran B 

//Created on: 
//Description: This function helps to getting value from userName
//Parameters: 
/****************************************************/
function getUserName(){
  
    Fasting = reviewOrder.WinFormsObject("lblReviewFasting");
    reviewOrder = process_SPM.WinFormsObject("frmSPM").WinFormsObject("TableLayoutPanel1").WinFormsObject("TabControl").WinFormsObject("tabReview").WinFormsObject("TableLayoutPanel5").WinFormsObject("Panel3").WinFormsObject("TableLayoutPanel26");
    Username = reviewOrder.WinFormsObject("lblUsername");
    
    Log.Message("Getting the Username");
    _commonFunctions.getText(Username);
}

function getSpecimenCodeValue(row,cell){
  process_SPM = Sys.Process("SPM");
//var path_dgvReviewSpecimen = "WinFormsObject(\"dgvReviewSpecimens\")";
  process_SPM.WaitWinFormsObject(path_dgvReviewSpecimen,2000);
  dgvReviewSpecimen = process_SPM.Find("Name",path_dgvReviewSpecimen,20);
  return dgvReviewSpecimen.wValue(row, cell);
}

function getTestCodeValue(row,cell){

  dgvReviewTests = process_SPM.Find("Name",path_dgvReviewTests,10);
  return dgvReviewTests.wValue(row,cell);
  
}

function closeScrollabelWindow(){
  
  ScrollableWindow = process_SPM.Find("Name",path_ScrollableWindow,10);
  ScrollableWindow.Close();
}
/****************************************************/
//Function Name: verify_ScrollableInfo
//Created By: Raja Singh 

//Created on: 
//Description: This function helps to getting value from userName
//Parameters: 
/****************************************************/
function verify_ScrollableInfo(value){
  
   process_SPM = Sys.Process("SPM");
   var textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
   textBox_Generic.Keys("T");
   var scrollableInfo= process_SPM.Find("Name",path_ScrollableTest,10);
   var grid =process_SPM.Find("Name",path_ReviewScrollableTest,10);
   if(aqString.Compare(grid.wValue(0, 1),value,true)){
     Log.Message("Successfully Verify that test 0135 is flagged UDEL")
   }
   else{
     Log.Message("Failed Verify that test 0135 is flagged UDEL")
   }
    scrollableInfo.Close();
    textBox_Generic.SetText("");  
}

function verify_SpecimensNotFlaged(v1,v2,v3){
  
   process_SPM = Sys.Process("SPM");
   var ReviewSpecimens = process_SPM.Find("Name",path_ReviewSpecimens,10);
   var grid =process_SPM.Find("Name",path_ReviewSpecimens,10);
   specimens1=grid.wValue(0, 0);
   specimens2=grid.wValue(1, 0);
   specimens3=grid.wValue(2, 0);
   _assertions.CheckEquals(specimens1,v1);
   _assertions.CheckEquals(specimens2,v2);
   _assertions.CheckEquals(specimens3,v3);
   Log.Message("Successfully Verify that Specimens are NOT flaged")
   }

function Verify_AccessionUnlocked(value){
  try{
     process_SPM = Sys.Process("SPM");
    textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
    _commonFunctions.actionControlU(textBox_Generic);
    textBox_OtherReason = process_SPM.Find("Name",path_txtOtherReason,10);
    textBox_OtherReason.SetText("TEST");
    process_SPM.Find("Name",path_SpecimensRequestedLabelOkButton,10).Click();
    UnlockMessage = process_SPM.Find("Name",path_UnlockMessage,10);
    UnlockMessagetext=UnlockMessage.Text;
    _assertions.CheckEquals(UnlockMessagetext,value)
    Log.Message("Successfully verify Accession Unlocked Message")
    }
    catch(e){
      Log.Message("Failed to verify Accession Unlocked Message")
      Log.Error(e)
    }
  
}
function click_OnReviewOrder(){
 
    var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);
    var tabControl = table.TabControl;
    tabControl.ClickTab("  Review Order  ");
    Log.Checkpoint("Successfully clicked on Review Order")
    okButton=process_SPM.Find("Name",path_OkButton,10);
    if(okButton.Exists){
      okButton.Click();
    }  
}

function verifyPopup(){
  
    var AlertMessage;
  
    process_SPM.WaitWinFormsObject(path_AlertPopup,2000);
    alertPopup = process_SPM.Find("Name",path_AlertPopup,10);
    alertMessage = process_SPM.Find("Name",path_AlertMessage,10);
    alertOk = process_SPM.Find("Name",path_btnAlertOk,10);
    
    if(alertPopup.Exists){
    Log.Checkpoint("Alert Message is present");
    
    AlertMessage = _commonFunctions.getText(alertMessage, "Alert Text Message")
    
    } else {
    Log.Error("Expected Alert Popup is not present");
    }
    
    return AlertMessage;
}

function click_btnAlertOk(){
    
  alertOk = process_SPM.Find("Name",path_btnAlertOk,10);
      
  _commonFunctions.click(process_SPM.Find("Name",path_btnAlertOk,10),"Ok Button");            
  
}

function popupOrderUpdateSummary()
{
  process_SPM = Sys.Process("SPM");
  popupSummary = process_SPM.Find("Name",path_popupSummary,10);
  
  var txt = popupSummary.Text;
    Log.Message(txt);

  popUpOKBtn = process_SPM.Find("Name",path_popUpOKBtn,10);

  _commonFunctions.click(popUpOKBtn,"Order summary ok button");
  
  return txt;

}


function closePopup(){
  
    var subString, AccessionID;
       
    let popup = process_SPM.Find("Name",path_AccessionPopup,10);
    
    if(popup.Exists){
      
    let btnOK = process_SPM.Find("Name",path_AccessionPopupBtnOK,10);
     _commonFunctions.click(btnOK,"Accession popup Ok button");
    }
    
    Log.Message("Accession Submited");

}


function scrollableTestWindow(){
  
  process_SPM = Sys.Process("SPM");
  textBox_Generic = process_SPM.Find("Name",path_textboxGeneric,10);
  
  textBox_Generic.Keys("T");
}


function closeScrollableTestWindow(){
  process_SPM = Sys.Process("SPM");
  var popup = process_SPM.Find("Name",path_scrollableTable,10);
  popup.Close();
}


function getFlagValue(rowNum,columnNumber,value,test){
  
  var grid, path_div, row_grid;
  process_SPM = Sys.Process("SPM");
  grid = process_SPM.Find("Name","WinFormsObject(\"dgvReviewTests\")",20);
  Log.Message(grid.Exists);
  if(aqString.Compare(grid.wValue(rowNum, columnNumber),value,true)){
     Log.Message("Successfully Verify that test "+test+" is flagged "+value);
   }
   else{
     Log.Message("Failed Verify that test "+test+" is flagged "+value);
   }
  
}


function verifySpecimen(){
  process_SPM = Sys.Process("SPM");
  var path_dgvReviewSpecimen = "WinFormsObject(\"dgvReviewSpecimens\")";

  dgvReviewSpecimen = process_SPM.Find("Name",path_dgvReviewSpecimen,10);
  return dgvReviewSpecimen.wValue(0, 5);

}

function verifyTest(){
  process_SPM = Sys.Process("SPM");

  var path_dgvReviewTests =  "WinFormsObject(\"dgvReviewTests\")";
  dgvReviewTests = process_SPM.Find("Name",path_dgvReviewTests,10);
  return dgvReviewTests.wValue(0, 3);
  //row_grid = _commonFunctions.select_RowFromDataGrid(grid, "*Code*", "*0202*");
  
}

function popupProblemTicketNotice(){
  process_SPM = Sys.Process("SPM");
  popUpOKBtn = process_SPM.Find("Name",path_popUpOKBtn,10);
  _commonFunctions.click(popUpOKBtn,"ProblemTicketNotice Ok Button");
}

function assertExists(element){

    process_SPM.WaitWinFormsObject(path_textboxGeneric,1000);
    var elementPath = process_SPM.Find(["Name","WndCaption","VisibleOnScreen"],[path_textboxLabel,element,"true"],10);
    elementPath.WaitProperty("WndCaption",element,2000);
    if(elementPath.Exists){
      Log.Message("Element located in the \"Review Order Page\"")
    }else{
      Log.Error("Element does not exist in the \"Review Order Page\"")
    }
}

function getTestFlag(ColumnName, Value){
  
    var testCode = process_SPM.Find("Name",path_ReviewScrollableTest,20);
    
    var flagValue = _commonFunctions.retriveFlagValues(testCode,ColumnName,Value);

    return flagValue;
}

function getSpecimenGrid(){
  process_SPM = Sys.Process("SPM");

  process_SPM.WaitWinFormsObject(path_dgvReviewTests,2000);
  dgvReviewTests = process_SPM.Find("Name",path_dgvReviewTests,10);
  return dgvReviewTests;
}

function verifyTestFlag(ColumnName, ColumnValue, TargetColumnName, Action){

  dgvReviewTests = process_SPM.Find("Name",path_dgvReviewTests,10);
  
  return _commonFunctions.performActionOnGrid(dgvReviewTests, ColumnName,ColumnValue,TargetColumnName,Action); 
  
}

function verifySpecimendata(ColumnName, ColumnValue, TargetColumnName, Action){
  dgvReviewSpecimen = process_SPM.Find("Name",path_dgvReviewSpecimen,10);
  
  return _commonFunctions.performActionOnGrid(dgvReviewSpecimen, ColumnName,ColumnValue,TargetColumnName,Action); 
}