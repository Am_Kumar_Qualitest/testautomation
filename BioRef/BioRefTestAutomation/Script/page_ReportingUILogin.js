﻿//USEUNIT _commonFunctions
//USEUNIT _config

var browser;
path_UserName = "Textbox(\"control_22\")";
path_Password = "PasswordBox(\"control_27\")";
path_LoginButton = "Button(\"control_32\")";

function enterval_UserName(username){
    Sys.Browser("iexplore").BrowserWindow(0).Maximize();
   _commonFunctions.setText(getElement("Name",path_UserName),username,"UserName");
}

function enterval_Password(password){
  
   _commonFunctions.setText(getElement("Name",path_Password),password,"Password");
}

function clickLoginBtn(){
  
   getElement("Name",path_LoginButton).Click();
   
   Log.Message("Sucessfully clicked on Login Button")
}


function getElement(prop,value){
  
  if(_config.browser == btIExplorer){
      
      browser = Sys.Browser("iexplore");
  }

    var element = browser.Find([prop,"VisibleOnScreen"],[value,"true"],30);
    if(element.Exists){
      Log.Message("Element located in the \"Login Page\"")
    }else{
      Log.Error("Element does not exist in the \"Login Page\"")
    }
    
    return element;
}

function MaximizeBrowserWindows(){

   Sys.Browser("iexplore").BrowserWindow(1).Maximize();

}