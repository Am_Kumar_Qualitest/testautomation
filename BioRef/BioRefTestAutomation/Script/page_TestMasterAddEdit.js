﻿//USEUNIT _commonFunctions

var path_editWindow = "WinFormsObject(\"frmTestMaster\")";
var path_tabControl = "WinFormsObject(\"tabControlMain\")";
var path_GridTestActions =  "WinFormsObject(\"dgvTestActions\")";
var path_popupAddEdit = "WinFormsObject(\"frmTestActionAdd_Edit\")";
var path_txtTestCode = "WinFormsObject(\"txtTestCode\")";
var path_txtNewCode = "WinFormsObject(\"ltbCode\")";
var path_txtCondition = "WinFormsObject(\"ddlCondition\")";
var path_txtApplication = "WinFormsObject(\"ddlApplication\")";
var path_txtAction = "WinFormsObject(\"ddlAction\")";
var path_btnCancel = "WinFormsObject(\"btnCancel\")";
var path_routeTable = "WinFormsObject(\"dgvTestRoute\")";

function navigateTab(value){
      
      process_TMA = Sys.Process("TestMaster");
      process_TMA.WaitWinFormsObject(path_editWindow,3000);
      var Tabs = process_TMA.FindChild(["Name"], [path_tabControl], 5);
      Tabs.ClickTab(value);
      Log.Message("Sucessfully clicked on the tab "+value)
}

function clickEdit(ColumnName, ColumnValue){

      process_TMA.WaitWinFormsObject(path_editWindow,3000);
      var TestActions = process_TMA.FindChild(["Name"], [path_GridTestActions], 10);
      
      select_RowFromDataGrid(TestActions, ColumnName, ColumnValue);
}

function getTestCodeValues(value, property){
    
     process_TMA.WaitWinFormsObject(path_popupAddEdit,2000);
     var popupTestCode = process_TMA.FindChild(["Name"], [path_popupAddEdit], 2);
     var field, val;
     
    switch(value){
       
     case "Action":
          field = popupTestCode.Find("Name",path_txtAction,3);
          val = _commonFunctions.getWindowProperty(field,property,"Action TextBox");
     break;
     
     case "Application":
          field = popupTestCode.Find("Name",path_txtApplication,3);
          val = _commonFunctions.getWindowProperty(field,property,"Action TextBox");
     break;
     
     case "Condition":
          field = popupTestCode.Find("Name",path_txtCondition,3);
          val = _commonFunctions.getWindowProperty(field,property,"Condition ComboBox");
     break;
     
     case "Code":
          field = popupTestCode.Find("Name",path_txtNewCode,3);
          val = _commonFunctions.getWindowProperty(field,property,"Code TextBox");
     break;
     }
     
     Log.Message("Successfully extracted text from field "+value+" was "+val);
     return val;
}

function clickPopUpCancel(){
      
      process_TMA = Sys.Process("TestMaster");
      var popup = process_TMA.FindChild(["Name"], [path_popupAddEdit], 2);
      var BtnCancel = popup.FindChild(["Name"], [path_btnCancel], 3);
      
      _commonFunctions.click(BtnCancel, "Pop-Up Cancel Button");
}

function clickCancel(){
      
      process_TMA = Sys.Process("TestMaster");
      var BtnCancel = process_TMA.FindChild(["Name"], [path_btnCancel], 2);
      
      _commonFunctions.click(BtnCancel, "Cancel Button");
}

function select_RowFromDataGrid(dataGrid, columnName, columnValue){
  
  var process, Grid, ColumnIds, Values, RowIndex;
  
  for(var i =0;i<10;i++){
    if(dataGrid.wRowCount>0){
      break;
    }else{
      aqUtils.Delay(1000);
    }
  }
   
  Grid = dataGrid;
  ColumnIds = new Array (columnName);
  Values = new Array (columnValue);
  
  RowIndex = FindRowByMultipleValues (Grid, ColumnIds, Values);
  if (RowIndex >= 0){
        cellValue =  Grid.ClickCell(RowIndex, 7);
        Log.Message("Clicked on Edit link for the Test code value "+Values);
        }
  else{
    Log.Error ("Row the Test code value was not found.");}
}


function specimenDetailsTab(ColumnName, ColumnValue){
    var specimenTab = process_TMA.Find("Name",path_specimenTab,8);
    
    if(specimenTab.RowCount>0){
      Log.Message("Test code details are present under specimen details tab")
    }else {
    Log.Error("Test code details are not present under specimen details tab");
    }
    
  // select_RowFromDataGrid(specimenTab, ColumnName, ColumnValue);
  // Log.Message(_commonFunctions.retriveFlagValues(specimenTab, ColumnName, ColumnValue));
}


function specialRoutingTab(ColumnName, ColumnValue, TargetColumnName, Action){
    var routeTable = process_TMA.Find("Name",path_routeTable,8);
    
    return _commonFunctions.performActionOnGrid(routeTable, ColumnName,ColumnValue,TargetColumnName,Action);
}

function close_AddEditWindow(){
      
      var addEditWindow = process_TMA.FindChild(["Name"], [path_editWindow], 5);
      addEditWindow.Close();
      Log.Message("Sucessfully Add Edit Window closed");
}
