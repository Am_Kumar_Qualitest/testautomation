﻿//USEUNIT _commonFunctions
//USEUNIT quit_Browser

var process_SPM;
var path_Accession="WinFormsObject(\"txtSrcAccessionNo\")";
var path_SearchButton="WinFormsObject(\"btnSearch\")";
var path_SearchResult="WinFormsObject(\"dgvSearchResult\")";

/****************************************************/
//Function Name: search_ResultByAccession
//Created By: Raja Singh  

//Created on: 
//Description: This function helps to searching result by AccessionID
//Parameters: value
/****************************************************/
function search_ResultByAccession(value){
  
  process_SPM = Sys.Process("SPM");
  textbox_Accession = process_SPM.Find("Name",path_Accession,10);
  searchButton =process_SPM.Find("Name",path_SearchButton,10);
  _commonFunctions.setText(textbox_Accession,value,"Accession");
    _commonFunctions.actionTab(textbox_Accession,"Accession");
    searchButton.Click();
  }
  /****************************************************/
//Function Name: click_OnSearchResult
//Created By: Raja Singh  

//Created on: 
//Description: This function helps to clicking on search result
//Parameters:   
/****************************************************/
function click_OnSearchResult()
{
  process_SPM = Sys.Process("SPM");
  searchResult= process_SPM.Find("Name",path_SearchResult,10);
  searchResult.DblClick();
}