﻿//USEUNIT quit_SPMApplication
//USEUNIT quit_Browser
//USEUNIT _assertions

/****************************************************/
//Function Name: click
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to clicking on object
//Parameters: value
/****************************************************/
function click(elementPath, fieldName){
  
  elementPath.Click();
  
  Log.Message("Successfully clicked on the field: "+fieldName)
}
/****************************************************/
//Function Name: clickItem
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to select object from ItemList
//Parameters: elementPath, fieldName
/****************************************************/
function clickItem(elementPath, fieldName){
  
  elementPath.ClickItem(fieldName);
  
  Log.Message("Successfully selected the field: "+fieldName)
}
/****************************************************/
//Function Name: setText
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to set text in text field
//Parameters: elementPath, fieldName
/****************************************************/

function setText(elementPath,text,fieldName){
    
    var value;
  
    elementPath.SetText(text);
    
    if(fieldName == "Password"){
      
    value = "Password";
    }else{
      
    value = text;
    }
  
    Log.Message("Successfully entered \""+value+"\" in the field: "+fieldName);
}
/****************************************************/
//Function Name: actionTab
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to do Tab action
//Parameters: elementPath, fieldName
/****************************************************/
function actionTab(elementPath,fieldName){
try{
  elementPath.keys("[Tab]");
  
  Log.Message("Successfully performed Tab action on field: "+fieldName);
  }catch(e){
      
    Log.Message("Not able to perform single Tab action on the text field "+fieldName);
    Log.Error(e);
  }
}
/****************************************************/
//Function Name: actionDoubleTab
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to do DoubleTab action
//Parameters: elementPath, fieldName
/****************************************************/
function actionDoubleTab(elementPath, fieldName){

  elementPath.keys("[Tab][Tab]");
  
  Log.Message("Successfully performed Double Tab action on field: "+fieldName);
}
/****************************************************/
//Function Name: actionDoubleTab
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to do DoubleTab action
//Parameters: elementPath, fieldName
/****************************************************/
function actionShiftTab(elementPath,fieldName){
  
  elementPath.keys("![Tab]");
  
  Log.Message("Successfully performed Shift+Tab action on field: "+fieldName);

}
/****************************************************/
//Function Name: actionDoubleTab
//Created By: Yogeswaran B
//Created on: 
//Description: This function helps to do DoubleTab action
//Parameters: elementPath, fieldName
/****************************************************/
function actionEnter(elementPath,fieldName){
   
  elementPath.keys("[Enter]");
  
  Log.Message("Successfully performed Enter action on field: "+fieldName);
}
/****************************************************/
//Function Name: getText
//Created By: Yogeswaran B
//Created on: 
//Description: This function helps to get text
//Parameters: elementPath, fieldName
function getText(elementPath, fieldName){
  
  Log.Message("Successfully extracted text from the field: "+fieldName);
  
  return aqObject.GetPropertyValue(elementPath, "wText");
}

function getValue(elementPath, fieldName){
  
  Log.Message("Successfully extracted text from the field: "+fieldName);
  
  return aqObject.GetPropertyValue(elementPath, "Text");
}

function getWindowProperty(elementPath, property ,fieldName){
  
  Log.Message("Successfully extracted property "+property+" from the field: "+fieldName);
  
  return aqObject.GetPropertyValue(elementPath, property);
}

//Function Name: get4DigitRandomInt
//Created By: Raja singh  
//Created on: 
//Description: This function helps to generate 4 digit random integer
//Parameters: elementPath, fieldName
function get4DigitRandomInt(){

      let min = 1000, max = 9999;
      let randInt = Math.round(Math.random()*(max-min)+min);
      
      Log.Message("Successfully generated 4 digit random number with value: "+randInt);
      
      return randInt;
}
//Function Name: getGender
//Created By: Raja Singh  
//Created on: 
//Description: This function helps to get gender
//Parameters: elementPath, fieldName
function getGender(){
  
      let min = 0, max = 2; 
      let gender = new Array("MALE", "FEMALE","UNKNOWN");
      let randGender = gender[Math.round(Math.random()*(max-min)+min)];
      
      Log.Message("Successfully generated random gender with value: "+randGender);
      
      return randGender;
}
//Function Name: getDateOfBirth
//Created By: Raja Singh  
//Created on: 
//Description: This function helps to getting DateOfBirth
//Parameters: elementPath, fieldName
function getDateOfBirth(){
  
    let objTimestamp = aqDateTime.Now();
    let dob = aqDateTime.AddMonths(objTimestamp,-(Math.floor(Math.random() * 900)));
    let randDOB = aqConvert.DateTimeToFormatStr(dob, "%m%d%Y");
    
    Log.Message("Successfully generated random Date of Birth with value: "+randDOB);
    
    return randDOB;
}

function getDateOfCollection(days){
  
    let objTimestamp = aqDateTime.Now();
    let doc = aqDateTime.AddDays(objTimestamp,days);
    let dateDOC = aqConvert.DateTimeToFormatStr(doc, "%m%d%Y");
    
    Log.Message("Generated Date of Collection with value: "+dateDOC+" and with "+days);
    
    return dateDOC;
}


function getTimeOfCollectionInMinutes(minutes){
  
    let objTimestamp = aqDateTime.Now();
    let doc = aqDateTime.AddMinutes(objTimestamp,minutes)
    let timeTOC = aqConvert.DateTimeToFormatStr(doc, "%m%d%Y");
    
    Log.Message("Generated Time of Collection with value: "+timeTOC+" and with "+minutes);
    
    return timeTOC;
}


function randString(intLength)
{
  
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   var charactersLength = characters.length;
   
   for ( var i = 0; i < intLength; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function getStringValue(string, subString, length){
  
      let aString = string;
      let aSubString = subString;

      let findPosition = aString.indexOf(aSubString);
  
      if (findPosition != -1){
          Log.Message("A substring '" + aSubString + "' was found at position " + findPosition);
          }
        else{
          Log.Message("There are no occurrences of '" + aSubString + "' in '" + aString + "'.");
          }
    
      let finalString = aqString.SubString(string, findPosition, 25);
    
      Log.Message("Succesfully retrived string value is : "+finalString);
    
      return finalString;
}

function getDoctorName(){
  
      let min = 0, max = 8; 
      //let name = new Array("BOULTON, STACY", "BUDMAN, DANIEL",  "JEWELL, BRYAN",  "MARSDEN, MALCOLM", "MOORE, BRAD",  "PATEL, DIMPLE",  "PETCHENIK, NICOLE",  "POMBO, LEAH",  "TEST, AMAR");//"SIMONSEN, REBECCA",  "ZAKHAROV, EVGENII",

      //let name = new Array("ANDERSON, SAMANTHA", "HOLT, LISA", "JEWELL, BRYAN", "MOORE, BRAD", "PETCHENIK, NICOLE", "POMBO, LEAH", "RAJU, SIRISHA", "U");
      let name = new Array("ACCOUNT, INACTIVE", "GAYLE, CHRISTOPHER", "GOWDA, LOHITH", "LAB, DEVELOPMENT", "PANDA, SANTANU", "PATEL, CHINTAN", "PATEL, MILAN", "RASKIND, WENDY", "U");
      let randName = name[Math.round(Math.random()*(max-min)+min)];
      
      Log.Message("Generated random doctor name with value: "+randName);
      
      return randName;
}

function getRace(){
  
      let min = 0, max = 3; 
      let name = new Array("ASIAN", "UNKNOWN",  "OTHER",  "NOT PROVIDED");
      let randRace = name[Math.round(Math.random()*(max-min)+min)];
      
      Log.Message("Generated random Race name with value: "+randRace);
      
      return randRace;
}

function getEthnicity(){
  
      let min = 0, max = 4; 
      let name = new Array("HISPANIC", "NON-HISPANIC", "ASHKENAZI JEWISH",  "OTHER",  "NOT PROVIDED");
      let randEthnicity = name[Math.round(Math.random()*(max-min)+min)];
      
      Log.Message("Generated random Ethnicity name with value: "+randEthnicity);
      
      return randEthnicity;
}

function getFasting(){
  
      let min = 0, max = 2;
      //let name = new Array("FASTING", "NON-FASTING",  "UNKNOWN",  "NOT_PROVIDED");
      let name = new Array("FASTING", "NON-FASTING",  "UNKNOWN");
      let randFasting = name[Math.round(Math.random()*(max-min)+min)];
      
      Log.Message("Generated random Fasting name with value: "+randFasting);
      
      return randFasting;
}

function closeApplication(processName){
    
    if (! Sys.WaitProcess(processName).Exists ){
      Log.Message("The Process \""+processName+"\" does not not exists");
    }else{ 
    
    Sys.Process(processName).Terminate();
    
    Log.Message("Successfully closed the "+processName+" application process");
    }
    
}
//Function Name: actionControlUp
//Created By: Raja Singh  
//Created on: 
//Description: This function helps to perform Control+up
//Parameters: elementPath, fieldName
function actionControlUp(elementPath,element){

  Log.Message("Successfully Performing Control+Up action on "+element);
   
  elementPath.keys("^[Up]");
}
//Function Name: getSpecimenTrackingID
//Created By: Raja Singh  
//Created on: 
//Description: This function helps to Generate 13 Digit Specimen Tratcking ID
//Parameters: elementPath, fieldName
function getSpecimenTrackingID()
{
  
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
   var charactersLength = characters.length;
   
   for ( var i = 0; i <13; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}
//Function Name: actionControlD
//Created By: Raja Singh  
//Created on: 31/08/2020
//Description: This function helps to perform Control+D
//Parameters: elementPath, fieldName
function actionControlD(elementPath,element){

  Log.Message("Successfully Performing Control+D action on "+element);
   
  elementPath.keys("^d");
}

//Function Name: actionControlD
//Created By: Raja Singh  
//Created on: 31/08/2020
//Description: This function helps to perform Control+D
//Parameters: elementPath, fieldName
function actionControlU(elementPath,element){

  Log.Message("Successfully Performing Control+D action on "+element);
   
  elementPath.keys("^u");
}

function select_RowFromDataGrid (dataGrid, columnName, columnValue)
{
  var process, Grid, ColumnIds, Values, RowIndex;
  ColumnIds = new Array (columnName);
  Values = new Array (columnValue);
  
  Grid = dataGrid;  
  RowIndex = FindRowByMultipleValues (Grid, ColumnIds, Values);
  
  if (RowIndex >= 0)
    return Grid.ClickCell(RowIndex, columnName);
        
  else
    Log.Error ("Row was not found.");
}

function getValueFromDataGrid (dataGrid, columnName, columnValue)
{
  var process, Grid, ColumnIds, Values, RowIndex;
   
  Grid = dataGrid;
  ColumnIds = new Array (columnName);
  Values = new Array (columnValue);
  
  RowIndex = FindRowByMultipleValues (Grid, ColumnIds, Values);
  if (RowIndex >= 0)
    return Grid.wValue(RowIndex, columnName);
        
  else
    Log.Error ("Row was not found.");
}

function FindRowByMultipleValues (Grid, Columns, Values)
{
  var match, i, j;
  // Iterate through grid rows
  for (i=0; i<Grid.wRowCount; i++)
  {
    match = true;
    // Check cell values in the specified columns
    for (j=0; j<Columns.length; j++)
      if (Grid.wValue(i, Columns[j]) != Values[j])
      {
        match = false; // Cell value differs from the specified one
        break;
      }
    if (match)
      return i;  // Row is found
  }
  return -1;  // Row is not found
}

function retriveFlagValues(dataGrid, columnName, columnValue){
  
  var process, Grid, ColumnIds, Values, RowIndex;
   
  Grid = dataGrid;
  ColumnIds = new Array (columnName);
  Values = new Array (columnValue);
  
  RowIndex = FindRowByMultipleValues (Grid, ColumnIds, Values);
  if (RowIndex >= 0){
    cellValue =  Grid.wValue(RowIndex, 3);}
        
  else{
    Log.Error ("Row was not found.");}
    
    return cellValue;
}

function assertGrid(gridName, arr)
{        
    //Traversing Rows
    for (var i = 0; i < gridName.wRowCount; i++)
    {
        //Traversing its child rows
        assertGridRows(gridName, i, arr);
    }
}

function assertGridRows(gridName, rowIndex, arr)
{

    //Iterate the cells
    for (var i = 0; i < 1; i++)
    {
      //Getting the cell value
      cellValue = gridName.wValue(rowIndex, i);

      //Log.Message(cellValue);
      
      arr.push(cellValue);
    }

    //checking for the child views
    if (gridName.wChildViewCount(rowIndex)>0)
    //iterating through child views
    for (var i = 0; i < gridName.wChildViewCount(rowIndex); i++)
    //getting data from the child view
    assertGrid(gridName.wChildView(rowIndex,i), arr);
}

function actionCtrlU(elementPath,fieldName){
try{  
  elementPath.keys("^u");
 
  Log.Message("Successfully performed Ctrl+U action on field: "+fieldName);
}catch(e){
     
    Log.Message("Not able to perform ctrl+U action on the text field "+fieldName);
    Log.Error(e);
  }
}

function getJSONData(jsonMessage,value,property){
 
 var json = JSON.parse(jsonMessage);
 
 var num;
 
    for(var i=0; i<json.Tests.length; i++){

        if(aqString.Trim(json.Tests[i][property]) == value){
        break;
        }
    }
    
    return json.Tests[i].ActionType;
}

function getJSONDataOfAllField(jsonMessage,mainField,fieldName){
 
 var json = JSON.parse(jsonMessage);
 
 if(aqString.GetLength(json) != 0){

  return aqString.Trim(json[mainField][fieldName]);
 
 }

}


function navigateToOtherPages(tabName){
  
    var process_SPM = Sys.Process("SPM");
    var table = process_SPM.FindChild(["Name"], ["WinFormsObject(\"TableLayoutPanel1\")"], 10);
    var tabControl = table.TabControl;
    tabControl.ClickTab("*"+tabName+"*");
  
    Log.Message("Successfully navigated to : "+tabName+" tab");
}

function doubleClk_RowFromDataGrid (dataGrid, columnName, columnValue)
{
  var process, Grid, ColumnIds, Values, RowIndex;
  ColumnIds = new Array (columnName);
  Values = new Array (columnValue);
  
  Grid = dataGrid;  
  RowIndex = FindRowByMultipleValues (Grid, ColumnIds, Values);
  
  if (RowIndex >= 0){
    return Grid.DblClickCell(RowIndex, columnName);
        Log.Message("Sucessfully clicked on the Edit link of "+columnValue)}
  else{
    Log.Error ("Row was not found.");
    }
}
function getValueFromGrid (dataGrid, columnName, columnName1,columnValue)
{
  var process, Grid, ColumnIds, Values, RowIndex;
   
  Grid = dataGrid;
  ColumnIds = new Array (columnName);
  Values = new Array (columnValue);
  
  RowIndex = FindRowByMultipleValues (Grid, ColumnIds, Values);
  if (RowIndex >= 0)
    return Grid.wValue(RowIndex, columnName1);
        
  else
    Log.Error ("Row was not found.");
}

function performActionOnGrid(Grid, ColumnName, ColumnValue, TargetColumnName, Action){
    
    var reqColumn,targetColumn,rowValue;
    
    for(var i =0;i<10;i++){
    if(Grid.Rows.Count>0){
      break;
    }else{
      aqUtils.Delay(1000);
    }
    }

    if(Grid.RowCount > 0){
      
      reqColumn = findColumnIndex(Grid, ColumnName);
      rowValue = findRowIndex(Grid, reqColumn, ColumnValue);
      targetColumn = findColumnIndex(Grid, TargetColumnName);
    }
    
    switch(Action){
      
    case "Click":
    clickGridCell(Grid,rowValue,targetColumn);
    break;
    
    case "DoubleClick":
    doubleClickGridCell(Grid,rowValue,targetColumn);
    break;
    
    case "Text":
    return getGridCellValue(Grid,rowValue,targetColumn);
    break;
    }
}

function findColumnIndex(Grid, ColumnName)
{
    var i = -1;

    for(i=0;i<Grid.Columns.Count;i++){
      if( aqString.Trim(Grid.Columns.Item(i).HeaderText.OleValue) == ColumnName){
        ColumnIndex =  i;
        break;
      }
    }
    
    if(i == -1){
      
    Log.Error("Not able to find the column name in the Grid")
    } else {
      
    return ColumnIndex;
    }
}

function findRowIndex(Grid, ColumnIndex, ColumnValue)
{
    var i = -1;

    for(var i=0;i<Grid.RowCount;i++){
      if( aqString.Trim(Grid.wValue(i, ColumnIndex)) == ColumnValue){
        RowIndex =  i;
        break;
      }
    }
    
    if(i == -1){
      
    Log.Error("Not able to find the given column value in the Grid")
    } else {
      
    return RowIndex;
    }
}

function clickGridCell(GridName,RowIndex, ColumnIndex){
  
  GridName.ClickCell(RowIndex, ColumnIndex);
  
  Log.Message("Successfully performed click action on the cell "+"("+RowIndex+","+ColumnIndex+")");
}

function doubleClickGridCell(GridName,RowIndex, ColumnIndex){
  
  GridName.DblClickCell(RowIndex,ColumnIndex);
  
  Log.Message("Successfully performed double click action on the cell "+"("+RowIndex+","+ColumnIndex+")");
}

function getGridCellValue(GridName,RowIndex, ColumnIndex){
  
  Log.Message("Successfully retrived the value from the cell "+"("+RowIndex+","+ColumnIndex+")");
  
  CellValue = GridName.wValue(RowIndex, ColumnIndex);
  
  return CellValue.ToString().OleValue;
}
