﻿//USEUNIT _commonFunctions
//USEUNIT _config
//USEUNIT quit_Browser

var process_SPM, browser;

var path_alertCloseApp = "Window(\"*\", \"Close Application\", 1)";
var path_alertOKBtn = "Window(\"Button\", \"OK\", 1)";
/****************************************************/
//Function Name: quitSPMApplication
//Created By: Yogeswaran B  

//Created on: 
//Description: This function helps to quit SPM Application
//Parameters: value
/****************************************************/
function quitSPMApplication(){
  
    Log.Message("Successfully closing the SPM Application");
  
    _commonFunctions.closeApplication("SPM");
}

function close_SPMPopup(){
  
if(_config.browser == btIExplorer){
      
      browser = Sys.Browser("iexplore");
  }
  
    if(browser.Page("*SPMClientApp*").Exists){
      
      browser.Page("*SPMClientApp*").Close();
    }
}

function closeFormSPM(){ 

  process_SPM = Sys.Process("SPM");
  process_SPM.WinFormsObject("frmSPM").Close();
  var alertCloseApp = process_SPM.Find("Name",path_alertCloseApp,10);
  var alertOKBtn = process_SPM.Find("Name",path_alertOKBtn,10);

  if(alertCloseApp.Exists){
    
    _commonFunctions.click(alertOKBtn,"OK Button");
  }
}